<?php
require ("config/get_main.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="generator" content="EXploresukabumi" >
    <meta name="description" content="temukan potongan surga yang hilang  sukabumi di sukabumi ,sukabumi terkenal akan kekayaan alam dan budayanya yang mempesona setiap pasang mata ">
    <meta name="keywords" content="exploresukabumi,sukabumi,explore,pariwisata,budaya,musik,kuliner,alam,geopark,pantai,gunung,pegunungan,bebas,alam">
    <meta name="author" content="syntac">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="tQ7xEnvjr7UdaYnzNYfRL0tX3wPa6TbOwkx__s2JZgk" />
    <meta property="og:url" content="" class="meta-url">
    <meta property="og:title" content=""   class="meta-title">
   <title>Penginapan - Explorer Sukabumi</title>
    <link rel="icon" type="img/ico" href="assets/image/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/css/main.style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/destination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
      <link href="assets/css/destination-data.css" rel="stylesheet">
    <link href="assets/css/destination.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="assets/css/homestay.css">
    <script src="./assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tmpl.min.js"></script>
     <script>
        jQuery(document).ready(function() {
            $(".meta-url").attr('content',document.URL);
            $(".meta-title").attr('content',$("title").text());
        });
    </script>

</head>

<body>
    <header id="header" class="header header-devault">
        <div class="container">
            <div class="logo"><a href="index.php" ><img src="assets/image/logo.png" alt=""></a></div>
            <div class="navbar">
                <button class="navbar-toggle devault-button-nav" nav-show="true"><span></span><span></span><span></span></button>
                <nav>
                    <ul>
                        <li class="active uppercase" class="active"><a href="index.php">beranda</a></li>
                        <li class="uppercase"><a href="destinations.php">destinasi</a></li>
                        <li class="uppercase"><a href="gallery.php" class="uppercase">galeri</a></li>
                        <li class="uppercase"><a href="news-list.php" class="uppercase">berita</a></li>
                        <li class="uppercase akomodasi pointer activa" ><span style="color: #696969 !important">akomodasi</span> 
                            <ul class="capital">
                                <li ><a href="homestay.php" title="">penginapan</a> </li>
                                <li> <a href="transportation.php" title="">transportasi</a> </li>
                            </ul>
                        </li>
                        <li class="p-0 search-unres-s">
                            <button type="button" class="search-unresponsive" active="false"><i class="fa fa-search "></i></button>
                        </li>

                        <li class="search-res">
                            <div class="ci">
                                <input type="text" name="" placeholder="Cari disini">
                                <button type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>



    <div class="list-type">
       <div class="container py-20">
            <h6 class="fs-16"  style="color: #717171 !important"><span>Menampilkan</span>: <label for="sel-data" ></label><select id="sel-data" name="sel-data" class="pointer list-dat-type fs-16 pr-20" style="color: #717171 !important; background: transparent;">
              <option class="capital fs-14" style="color: #717171 !important">semua akomodasi</option>
              <option class="capital fs-14" style="color: #717171 !important" >hotel</option>
              <option class="capital fs-14" style="color: #717171 !important">hostel</option>
              <option class="capital fs-14" style="color: #717171 !important">apartememt</option>

              
            </select> <i class="fa fa-chevron-down fs-13" style="left: -15px ; z-index: -1;"></i>
            </h6>
       </div>
    </div>

    <div class="content-data-list">
    <div class="container">
       <div class="menu-filter-data">
       <div class="container-list">
          <div class="check">
            <div class="list-check pb-10">
              <h3 class="fs-14 capital py-10">check in</h3>
              <div style="width: 100%;overflow: hidden; padding: 0; border:1px solid #BABABA">
              <input type="text" name="" class="py-8 " placeholder="HH/BB/TT" id="start" style="border:0 !important;">
            </div>
            </div>
             <div class="list-check pb-10">
              <h3 class="fs-14 capital py-10">check out</h3>
             <div style="width: 100%;overflow: hidden; padding: 0; border:1px solid #BABABA">
                <input type="text" name="" class="py-8 " placeholder="HH/BB/TT" id="end" style="border:0 !important">
             </div>
            </div>
          </div>

          <div class="data-title px-10 py-10 pt-20">
            <h3 class="fs-14 capital">filter pencarian</h3>
          </div>

          <div class="acordion-list">
            <div class="acord-data px-10">
              <button type="" class="acord-onoff fs-14  py-7 px-8"><span class="capital fs-14">pilihan akomodasi</span> <i class="fs-19 fa fa-chevron-down data-close-arc"></i></button>

              <div class="acord-content unacr">
                <div class="list-type">
                  <label><input type="radio" name="val-name"> <span class="fs-14 capital">vila</span></label></div>
                    <div class="list-type"><label><input type="radio" name="val-name"> <span class="fs-14 capital">hotel</span></label></div>
                    <div class="list-type"><label><input type="radio"  name="val-name"> <span class="fs-14 capital">hostel</span></label></div>
                    <div class="list-type"><label><input type="radio" name="val-name"> <span class="fs-14 capital">apartement</span></label>
                </div>
              </div>
            </div>
          </div>

          <div class="acordion-list">
            <div class="acord-data px-10">
              <button type="" class="acord-onoff fs-14  py-7 px-8"><span class="capital fs-14">bintang</span> <i class="fs-19 fa fa-chevron-down data-close-arc "></i></button>

              <div class="acord-content unacr">
                <div class="rate-start">
                  <ul>
                    <li><label> <input type="checkbox" name=""> <i class="fa fa-star"></i> </label></li>
                    <li><label> <input type="checkbox" name=""> <i class="fa fa-star"></i><i class="fa fa-star"></i> </label></li>
                    <li><label> <input type="checkbox" name=""> <i class="fa fa-star"></i> <i class="fa fa-star"></i><i class="fa fa-star"></i></label></li>
                    <li><label> <input type="checkbox" name=""> <i class="fa fa-star"></i> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></label></li>
                    <li><label> <input type="checkbox" name=""> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> </label></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>


           <div class="acordion-list">
            <div class="acord-data px-10">
              <button type="" class="acord-onoff fs-14  py-7 px-8"><span class="capital fs-14">fasilitas</span> <i class="fs-19 fa fa-chevron-down data-close-arc"></i></button>

              <div class="acord-content unacr">
               <ul>
                 <li><label class=""><input type="checkbox" name=""> <span class="capital">wifi</span></label></li>
                  <li><label class=""><input type="checkbox" name=""> <span class="capital">lift</span></label></li>
                   <li><label class=""><input type="checkbox" name=""> <span class="capital">air panas</span></label></li>
                    <li><label class=""><input type="checkbox" name=""> <span class="capital">pusat kebugaran</span></label></li>
                     <li><label class=""><input type="checkbox" name=""> <span class="capital">parkir</span></label></li>
                      <li><label class=""><input type="checkbox" name=""> <span class="capital">fasilitas rapat</span></label></li>
               </ul>
              </div>
            </div>
          </div>



       </div>
     </div>
     <div class="menu-book-list" id="book-list">
       
<!--dont change entry from this div tag -->
     
<div  class="list-book" id="booking-me-list">

</div> 


    </div>
    </div>
    </div>





    <div class="container">
    <div class="page">
        <nav id="page">
            <ul>
                <li>
                    <button type="button" class="button-trek" id="left" disabled=""><i class="fa fa-angle-left"></i></button>
                </li>
                <li>
                    <ul id="page-list-act">
                        <li id="page-button" class="page-button">
                            <button type="button">1</button>
                        </li>
                    </ul>
                </li>
                <li>
                    <button type="button" class="button-trek" id="right"><i class="fa fa-angle-right"></i></button>
                </li>
            </ul>
        </nav>
    </div>
</div>





   <footer class="footer">
      <div class="container">
         <div class="foot-logo ">
            <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.php" title=""><img src="assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
            <div class="some">
               <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
               <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3>
            </div>
         </div>
         <div class="foot-nav">
            <div class="nav-title mb-20">
            </div>
            <div class="nav-body">
               <nav>
                  <ul>
                     <li>
                       <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.php" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.php" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.php" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.php" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.php" title="news">transportasi</a> </li>
                        </ul>
                     </li>
                     <li>
                        <ul>
                           <li class="capital"><span>hubungi kami</span></li>
                           <li>
                              <p>Telepon</p>
                           </li>
                           <li>
                              <p>+123 456 789 </p>
                           </li>
                           <li>
                              <p>Email</p>
                           </li>
                           <li>
                              <p>eksukabumi@hotmail.com</p>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <ul>
                           <li class="capital"><span>sosial media</span></li>
                           <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                           <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                           <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                           <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                        </ul>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
      </div>
   </footer>
   <div class="con-copy">
      <input type="text" class="" name="" id="value-link">
   </div>

   <div class="search-popup inactive">
      <button type="button" class="fa fa-close-o dismis-c"></button>
      <div class="container-v">
         <div class="filter">
            <div class="search">
               <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
               <button type="button" class="fa fa-close-o remove-text"></button>
               <button type="button" class="fa fa-to-go button-w"></button>
            </div>
         </div>
         <div class="filter-data ">
            <div class="pt-20 pb-10">
               <span class="fs-14 c-black ">Filter</span>
            </div>
            <div class="filter-content">
               <ul>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Semua</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Destinasi</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Berita</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Akomondasi</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Hotel</span></label>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="ads-popup-hotel inactive">
      <div class="ads-container">
         <div class="ads-image">
            <img src="" alt="">
         </div>
         <div class="ads-content">
            <h3 class="oswald fs-18 uppercase pb-10 hotel-name">hotel</h3>
            <ul class="list-none ">
               <li>
                  <p class="fs-14 hotel-desription"></p>
               </li>
               <li class="py-5 pt-10 list-none py-5">
                  <h6> <i class="fa fa-cutlery fs-17"></i><span class="fs-14 pl-10">gratis makan</span> </h6>
               </li>
               <li class="py-4 list-none py-5">
                  <h6> <i class="fa fa-map-marker fs-17"></i><span class="fs-14 pl-10 jarak-to"></span> </h6>
               </li>
               <li class="py-5 parker">
                  <strong class="parking oswald fs-11">
                     <p>p</p>
                  </strong>
                  <span class="pl-10 fs-14">Parkir</span>
               </li>
            </ul>
            <div class="button-chart">
               <span class="chart">
                  <p class="fs-14 capital">mulai dari</p>
                  <p class="fs-14 "><span class="rp harga"></span>/malam</p>
               </span>
               <button type="button" dismis-hotel class="py-7 fs-14 px-10 capital book">pesan sekarang</button>
            </div>
         </div>
         <button type="" dismis-hotel class="dismis  closed fa fa-times fs-17"> </button>
      </div>
      <div class="popup-closer-area"></div>
   </div>
   <div class="ads-popup-rental inactive">
      <div class="container-ads">
         <div class="area-title pt-15 pb-">
            <h3 class="fs-18 text-left capital rental-name"  ></h3>
         </div>
         <div class="area-list">
            <ul class="list-none">
               <li class="phone"><a href="" title=""><i class="fa fa-phone fs-25 w-25 mr-20 py-10 " aria-hidden="true" style="top: 5px;"></i> <span></span></a></li>
               <li class="twitter"><a href="" title=""><i class="fa fa-twitter fs-25 w-25 mr-20 py-10 " aria-hidden="true" style="top: 5px;"></i> <span></span></a></li>
               <li class="facebook"><a href="" title=""><i class="fa fa-facebook fs-25 w-25 mr-25 py-10 " aria-hidden="true" style="top: 5px;"></i><span class="pl-8"></span></a></li>
            </ul>
         </div>
         <div class="area-button pt-30 pb-10">
            <button type="button" class="fs-25 fa fa-times" dismis="rental"></button>
         </div>
      </div>
      <div class="area-closer"></div>
   </div>




   <!-- popup share -->


   <div class="share-popup in inactive">
     <div class="share-container">
       <div class="share-area">
       <div class="share-title pb-15">
         <h3 class="fs-20 oswald text-left capital">bagikan ke</h3>
       </div>
       <div class="share-content">
         <ul class="list-none">
           <li><button type="" class="share-to to-fb fs-14 capital" to="fb"><i class="fs-18 fa fa-facebook"></i> facebook </button></li>
           <li><button type="" class="share-to to-tw fs-14 capital" to="tw"><i class="fs-18 fa fa-twitter"></i> twitter </button></li>
           <li><button type="" class="share-to to-ig fs-14 capital" to="ig"><i class="fs-18 fa fa-instagram"></i> instagram </button></li>
           <li><button type="" class="share-to to-gp fs-14 capital" to="gp"><i class="fs-18 fa fa-google-plus  "></i> google plus </button></li>
           <li><button type="" class="share-to to-url fs-14 capital" to="url"><i class="fs-18 fa fa-link"></i> salin url </button></li>
         </ul>
       </div> 
       <div class="buttton-close">
         <button type="button" onclick="close_share()"></button>
       </div>  
       </div>
     </div>
     <div class="close-share"></div>
   </div>




   <div class="popup">
      <div class="content-popup">
         <button type="button" class="dismis"><i class="fa fa-times"></i></button>
         <video src="" id="video-play" autoplay="" controls=""></video>
      </div>
      <div class="area-close"></div>
   </div>
   <div class="popup-rad ">
      <div class="image"><img src="assets/image/galery/galery-12.png" alt="">
         <button type="button" close="popup"><i class="fa fa-close"></i></button>
      </div>
      <div class="area-close"></div>
   </div>
   </body>
          <script src="./assets/js/action.js"></script>

<script src="assets/js/jquery-ui.js"></script>

<script id="hotel-list-data-js-tmpl" type="text/x-jQuery-tmpl">
     <div class="list-data">
               <div class="data-content">
                  <div class="image-data">
                     <img src="assets/image/post_images/${gambar}" alt="">
                  </div>
                  <div class="content-data">
                     <div class="title-data " style="padding-bottom: 8px; padding-top: 10px;">
                           <h3 class="uppercase oswald  capital nama-hotel" style="font-size: 20px;">${nama}</h3>
                        </div>
                     <div class="item-content">
                       
                        <ul>
                           <li> <div class="rate-area">
                  <div class='rate' >
                    <div class="sub-rate" rating='${rating}'>
                        <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div> </li>
                          <li><i  class="fa fa-map-marker"></i><span class="capital">${lokasi}</span> </li>
                          <li><i  class="fa fa-cutlery"></i><span class="capital">gratis makan</span> </li>
                          <li><i  class="fa fa-gps"></i><span class="capital" class="to-jarak">${jarak}</span> </li>
                        </ul>

                     </div>

                     <div class="item-content-data-b">
                       <div class="list-dec">
                         <div>
                           <span class="awal">RP.${harga_awal}</span>
                         </div>
                         <div>
                           <span class="akhir">RP.${harga_akhir}</span>
                         </div>
                         <div class="hiden">
                           <span class="deskripsi">${deskripsi}</span>
                           <span  class="jarak-data">${jarak}</span>
                         </div>

                         <button type="button" class="book-me-now capital">pesan sekarang</button>
                       </div>
                     </div>
                  </div>
               </div>
            </div>
   </script>

   <p class="data"></p>
    <script src="assets/js/owl.carousel.js"></script>
       <script src="assets/js/jquery.auto-complete.js"></script>
<script src=" assets/js/home-get.js"></script>


<script>
  $.ajax({
    url: 'config/show_post_homestay.php',
    success:function(result){
    
      console.log(result);
      
     var component  = $.parseJSON(result);

     send_to_get(component);



    }
  })



          jQuery(document).ready(function() {

setInterval(function(){
            $("#start,#end").datepicker({ locale: 'id',});

             var eq_legngth=$(".list-data").length;
              for (var  j= 1; j <= eq_legngth ; j++) {
      

            for (var i = 1; i <= 4 ; i++) {
               
                 var rate=$("#booking-me-list-"+j+" .list-data:nth-child("+i+")").find(".sub-rate").attr('rating');

                 $("#booking-me-list-"+j+" .list-data:nth-child("+i+")").find('.start').slice(0,rate).removeClass('unrate').addClass('rate');
            }

           }
         },200)
         });

          $(document).on('click', '.book-me-now', function(event) {

            $(".ads-popup-hotel").removeClass('inactive').addClass("active");
            $(".ads-popup-hotel .ads-image img").attr('src', $(this).closest('.list-data').find('img').attr('src'));
            $(".ads-popup-hotel .hotel-name").text($(this).closest('.list-data').find('.nama-hotel').text())
            $(".ads-popup-hotel .hotel-desription").empty().append($(this).closest('.list-data').find('.deskripsi').text().substr(0,100) + "...")
            $(".ads-popup-hotel .jarak-to").text($(this).closest('.list-data').find('.jarak-data').text())
            $(".ads-popup-hotel .harga").text($(this).closest('.list-data').find('.akhir').text())
          });

    

 

</script>

</html>








