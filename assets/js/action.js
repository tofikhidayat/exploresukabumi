$(".navbar-toggle").click(function(event) {
    if ($(this).attr('nav-show') == "true") {
        $(".navbar nav").addClass('active').removeClass('inactive');
        $(this).attr('nav-show', "false");
        $(this).addClass('active');
    } else {
        $(".navbar nav").removeClass('active').addClass('inactive');
        $(this).attr('nav-show', "true");
        $(this).removeClass('active');
    }
});
$(".js-error").hide();
$(document).ready(function() {
    for (var i = 0; i <= 200; i++) {
        $(".m-" + i).css('margin', i + 'px');
        $(".ml-" + i).css('margin-left', i + 'px');
        $(".mx-" + i).css('margin-left', i + 'px').css('margin-right', i + 'px');
        $(".my-" + i).css('margin-top', i + 'px').css('margin-bottom', i + "px");
        $(".mr-" + i).css('margin-right', i + 'px');
        $(".mt-" + i).css('margin-top', i + 'px');
        $(".mb-" + i).css('margin-bottom', i + 'px');
        $(".p-" + i).css('padding', i + 'px');
        $(".px-" + i).css('padding-left', i + 'px').css('padding-right', i + 'px');
        $(".py-" + i).css('padding-top', i + 'px').css('padding-bottom', i + 'px');
        $(".pl-" + i).css('padding-left', i + 'px');
        $(".pr-" + i).css('padding-right', i + 'px');
        $(".pt-" + i).css('padding-top', i + 'px');
        $(".pb-" + i).css('padding-bottom', i + 'px');
        $(".fs-" + i).css('font-size', i + 'px');
        $(".h-" + i).css('height', i + 'px');
        $("lh-" + i).css('line-height', i);
    }


    for (var i = 1; i <= 100; i++) {
        $(".list-banner:nth-child(" + i + ")").find('.square-title').text(i);
    }
    jQuery(document).ready(function($) {
        $('#banner-list').owlCarousel({
            loop: true,
            center: false,
            margin: 10,
            items: 4,
            lazyLoad: true,
            dots: false,
            nav: false,
            slideSpeed: 3000,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1
                },
                420: {
                    items: 3
                },
                550: {
                    items: 4
                },
                768: {
                    items: 4
                },
                1170: {
                    items: 5
                }
            }
        });
    });
    $(document).on('click', '.list-banner', function(event) {
        $(".list-banner").css('opacity', '.6');
        $(this).css('opacity', '1');
        $(".banner-title h1").text($(this).find('.to-title').text());
        $(".banner-desc p").text($(this).find('.to-desc-title').text())
        $(".banner-right").css('background-image', 'url("assets/image/' + $(this).find('.to-img').text() + '")');
        $(".banner-right").css('opacity', '0').css('transition', '1s ease');
        $("#target-link").attr('href', $(this).find('.target-data-link').text());
        $(".banner-right").css('opacity', '1').css('transition', '1s ease');
    });
});



jQuery(document).ready(function($) {
    "use strict";
    $('#list-image').owlCarousel({
        loop: true,
        center: false,
        items: 4,
        margin: 0,
        lazyLoad: true,
        autoplay: true,
        dots: true,
        nav: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        slideSpeed: 3000,
        responsiveRefreshRate: 200,
        navText: ["<i class='fa fa-chevron-left nav-car-left' ></i>", "<i class='fa fa-chevron-right nav-car-right'></i>"],
        responsive: {
            0: {
                items: 3
            },
            320: {
                items: 3
            },
            420: {
                items: 3
            },
            540: {
                items: 3
            },
            768: {
                items: 3
            },
            1170: {
                items: 3
            }
        }
    });
});
jQuery(document).ready(function($) {
    "use strict";
    $('#destination').owlCarousel({
        loop: true,
        center: false,
        items: 4,
        margin: 0,
        autoplay: true,
        dots: true,
        lazyLoad: true,
        nav: true,
        autoplayHoverPause: true,
        autoplayTimeout: 6000,
        slideSpeed: 3000,
        responsiveRefreshRate: 200,
        navText: ["<i class='fa fa-long-arrow-left nav-car-left' ></i>", "<i class='fa fa-long-arrow-right nav-car-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 1
            },
            420: {
                items: 1
            },
            540: {
                items: 2
            },
            768: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });
});
$(document).on('click', '.image-list', function(event) {
    $("#bg-list").find('img').attr('src', $(this).find('img').attr('src'));
    $("#bg-list").find('img').css('opacity', '0 !important').css('transition', '1s ease');
    $("#bg-list").find('img').css('opacity', '1 !important').css('transition', '1s ease');
});

$("#video-feed").click(function(event) {
    $(".popup").css("display", "flex");
    $(".popup .content-popup").css("height", "0px").css('width', '0px').css('transition', '0s ease').fadeOut(200);
    $(".popup .content-popup").css("height", "90%").css('width', '90%').css('transition', '.4s ease').fadeIn(100);;
    $("#video-play").attr('src', $(this).attr("target-video"));
    if ($(document).width() <= 550) {
        $("#video-play").css('transform', 'rotate(90deg)');
    } else {
        $("#video-play").css('transform', 'rotate(0deg)');
    }
});
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        dismis();
        close_share()
        close_rental();
        hotel_close()
    }
});


$(".dismis").click(function(event) {

    dismis();
});

function dismis() {
    $(".popup , .popup-rad").css("display", "none")
    $(".popup .content-popup").css("height", "0px").css('width', '0px').css('transition', '0s ease').fadeOut(200);
    $(".popup .content-popup").css("height", "0%").css('width', '0%').css('transition', '.4s ease').fadeIn(100);
    $("#video-play").attr('src', '');
}
$(document).scroll(function(event) {
    if ($(document).scrollTop() >= 100) {
        $("#header").removeClass('header-devault').addClass('header-scroll');
        $(".search-unres").removeClass('unscroll').addClass('scroll');

        $(".autocomplete-suggestions").removeClass('unscroll').addClass('scroll');
    } else {
        $("#header").removeClass('header-scroll').addClass('header-devault');
        $(".search-unres").removeClass('scroll').addClass('unscroll');
        $(".autocomplete-suggestions").removeClass('scroll').addClass('unscroll');
    }
});
jQuery(document).ready(function($) {
    $('#owl-galery').owlCarousel({
        loop: true,
        center: false,
        margin: 30,
        autoplay: false,
        dots: false,
        nav: false,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        slideSpeed: 3000,
        lazyLoad: true,
        responsiveRefreshRate: 200,
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 1
            },
            420: {
                items: 2
            },
            540: {
                items: 2
            },
            768: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });
});
$(document).on('click', '*[close=popup]', function(event) {
    $(this).closest('.popup , .popup-rad').css('display', 'none');
});



for (var i = 1; i <= 3; i++) {
    $(".list-galery:nth-child(" + i + ")").click(function(event) {
        $('.popup-rad').css('display', 'flex');
        $('.popup-rad').css('opacity', '0');
        $('.popup-rad').css('transition', '1s ease').css('opacity', '1');
        $(".popup-rad").css("display", "flex");
        $(".popup-rad img").css("height", "0px").css('width', '0px').css('transition', '0s ease').fadeOut(200);
        $(".popup-rad img").css("height", "100%").css('width', '100%').css('transition', '.4s ease').fadeIn(100);
        $('.popup-rad').find('img').attr('src', $(this).find('img').attr('src'));
        $(".popup-rad").find('.credit-dat').text($(this).find('.credit-to').text())
    });
}


$(document).on('click', '.area-close', function(event) {
    dismis();
});



$(".video-data-show").click(function(event) {
    $(".popup").css("display", "flex");
    $(".popup .content-popup").css("height", "0px").css('width', '0px').css('transition', '0s ease').fadeOut(200);
    $(".popup .content-popup").css("height", "90%").css('width', '90%').css('transition', '.4s ease').fadeIn(100);;
    $("#video-play").attr('src', $(this).attr("target-video"));
    if ($(document).width() <= 550) {
        $("#video-play").css('transform', 'rotate(90deg)');
    } else {
        $("#video-play").css('transform', 'rotate(0deg)');
    }
});



jQuery(document).ready(function($) {
    $('#video-feed-play').owlCarousel({
        loop: true,
        center: true,
        margin: 30,
        autoplay: true,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-long-arrow-left nav-car-left' ></i>", "<i class='fa fa-long-arrow-right nav-car-right'></i>"],
        autoplayHoverPause: true,
        autoplayTimeout: 3000,
        slideSpeed: 3000,
        lazyLoad: true,
        responsiveRefreshRate: 200,
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 1
            },
            420: {
                items: 1
            },
            540: {
                items: 1
            },
            768: {
                items: 1
            },
            1170: {
                items: 1
            }
        }
    });
});

//for goto\

$(".list-galery-data").click(function(event) {
    $('.popup-rad').css('display', 'flex');
    $('.popup-rad').css('opacity', '0');
    $('.popup-rad').css('transition', '1s ease').css('opacity', '1');
    $(".popup-rad").css("display", "flex");
    $(".popup-rad img").css("height", "0px").css('width', '0px').css('transition', '0s ease').fadeOut(200);
    $(".popup-rad img").css("height", "100%").css('width', '100%').css('transition', '.4s ease').fadeIn(100);
    //jumper data image
    $('.popup-rad').find('img').attr('src', $(this).attr('src'));
});

//setting searchbar unresponsive




jQuery(document).ready(function($) {
    $('.ci input').autoComplete({
        minChars: 1,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var choices = ['ActionScript', 'AppleScript', 'Asp', 'Assembly', 'BASIC', 'Batch', 'C', 'C++', 'CSS', 'Clojure', 'COBOL', 'ColdFusion', 'Erlang', 'Fortran', 'Groovy', 'Haskell', 'HTML', 'Java', 'JavaScript', 'Lisp', 'Perl', 'PHP', 'PowerShell', 'Python', 'Ruby', 'Scala', 'Scheme', 'SQL', 'TeX', 'XML'];
            var suggestions = [];
            for (i = 0; i <= destinasi.length; i++)
                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(destinasi[i]["nama"]);
            suggest(suggestions);
        }
    });
});


//scrolll to top action

$(document).scroll(function(event) {


    var sctarget = $(document).height() - $(window).height() - 1000;
    var winsc = $(document).scrollTop();

    if (winsc > 1000) {
        $(".to-top").removeClass('unscroll').addClass('scroll');
    } else {
        $(".to-top").removeClass('scroll').addClass('unscroll');
    }

});


$("#to-top ").click(function(event) {
    $("html , body").animate({
        scrollTop: 0
    }, 1000);
});




jQuery(document).ready(function() {
    $(".filter-content ul li:nth-child(1) ").find('input').attr('checked', 'true');
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        dismis_c();
    }
});

$(".ci").click(function(event) {
    active_w()
    $(".navbar nav").removeClass('active').addClass('inactive');
    $(".navbar-toggle").attr('nav-show', "true");
});



$(".dismis-c").click(function(event) {
    dismis_c();
})

function dismis_c() {
    $(".search-popup").removeClass('active').addClass('inactive')
    $(".navbar-toggle").removeClass('active');
    $(".navbar-toggle.devault-button-nav").attr('nav-show', 'true');

}
$(".text-input-filter").keyup(function(event) {
    if ($(this).val().length > 0) {
        $(".remove-text").css('opacity', '1');
    } else {
        $(".remove-text").css('opacity', '0');
    }

});

$(".remove-text").click(function(event) {
    $(".text-input-filter").val('');
    $(this).css('opacity', '0');
});


$(document).on('click', '.search-unresponsive', function(event) {

    active_w();

});

function active_w() {
    $(".search-popup").removeClass('inactive').addClass('active')
}


function doRandom(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


//like me 


$(".like-me").click(function(event) {
    var post_id = $(this).attr('post_id'); 
    if ($(this).attr('liked') == "false") {
        $(this).attr('liked', 'true');
        var send_type = "plus";
    } else {
        $(this).attr('liked', 'false');
        var send_type = "minus";
    }

    send_like(post_id,send_type);
});


///hotel-popup
$(document).on('click', '.book-me', function(event) {
    //show hotels popup
    $(".ads-popup-hotel").removeClass('inactive').addClass("active");
    //jumper data image
    $(".ads-popup-hotel .ads-image img").attr('src', $(this).closest('.list-data').find('img').attr('src'));
    //jumper data text
    $(".ads-popup-hotel .hotel-name").text($(this).closest('.list-data').find('.nama-hotel').text())
    $(".ads-popup-hotel .hotel-desription").text($(this).closest('.list-data').find('.description').text())
    $(".ads-popup-hotel .jarak").text($(this).closest('.list-data').find('.jarak').text())
    $(".ads-popup-hotel .harga").text($(this).closest('.list-data').find('.harga').text())
});
$(document).on('click', 'button[dismis-hotel]', function(event) {
    hotel_close()
});

$(document).on('click', '.popup-closer-area', function(event) {
    hotel_close()
});

function hotel_close() {
    $(".ads-popup-hotel").removeClass('active').addClass("inactive");
}



function close_rental() {
    $(".ads-popup-rental").removeClass('active').addClass('inactive');
}
$("button[dismis=rental]").click(function(event) {
    close_rental();
});

$(".book-rental").click(function(event) {
    $(".ads-popup-rental").removeClass('inactive').addClass('active');

    $(".ads-popup-rental .rental-name").text("hubungi" + $(this).closest('.list-data').find('.rental-name').text())
    $(".ads-popup-rental .phone span").text($(this).closest('.list-data').find('.phone').text())
    $(".ads-popup-rental .twitter span").text($(this).closest('.list-data').find('.twitter').text())
    $(".ads-popup-rental .facebook span").text($(this).closest('.list-data').find('.facebook').text())
});

$(".close-share").click(function(event) {
    close_share()
});




function close_share() {
    $(".share-popup").removeClass('active').addClass('inactive');
}

$(".share-me-please").click(function(event) {
    $(".share-popup").removeClass('inactive').addClass('active');
});
//setting popup and share to medsos
$(document).on('click', '.share-to', function() {
    if ($(this).attr('to') == "fb") {
        var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + document.URL, 'facebook-popup', 'height=350,width=600');
        if (facebookWindow.focus) {
            facebookWindow.focus();
        }
        close_share()
        return false;

    } else if ($(this).attr('to') == "tw") {
        var twitterWindow = window.open('https://twitter.com/share?url=' + document.URL, 'twitter-popup', 'height=350,width=600');
        if (twitterWindow.focus) {
            twitterWindow.focus();
        }
        close_share()
        return false;

    } else if ($(this).attr('to') == "ig") {
         var url = document.URL;
            var $temp = $("#value-link");
            $temp.select();
            document.execCommand("copy");
        
      var twitterWindow = window.open('https://www.instagram.com/', 'instagram-popup', 'height=350,width=600');
        if (twitterWindow.focus) {
            twitterWindow.focus();
        }



        close_share()
    } else if ($(this).attr('to') == "gp") {
         var twitterWindow = window.open('https://plus.google.com/share?url=' + document.URL, 'twitter-popup', 'height=350,width=600');
        if (twitterWindow.focus) {
            twitterWindow.focus();
        }



        


        close_share()
    } else if ($(this).attr('to') == "url") {
        var url = document.URL;
        var $temp = $("#value-link");
        $temp.select();
        document.execCommand("copy");

        close_share()
    } else {

    }

});

jQuery(document).ready(function($) {
    $("#value-link").val(document.URL);
});


$(".acord-onoff").click(function(event) {
$(this).closest('.content-data-list').find('.acord-content').slideUp("fast");


if ($(this).closest('.acordion-list').find('.acord-content').css('display') == "block") {
$(this).closest('.acordion-list').find('.acord-content').slideUp("fast");
$(".data-close-arc").css('transform', 'rotate(0deg)');
}
else
{
  $(this).closest('.acordion-list').find('.acord-content').slideDown("fast");
$(".data-close-arc").css('transform', 'rotate(0deg)');
$(this).closest('.acordion-list').find(".data-close-arc").css('transform', 'rotate(180deg)');
  
}
});



//chat
//$("head").append('<link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">');
$("head").append('<link rel="stylesheet" href="assets/css/chat.css">');
$("html").append('<script src="ajax/chat.js"></script>');
$("body").append('<div class="chat"> <div class="chat-button"> <button type="" class="fa "> <i class="fa fa-commenting-o"></i> </button> </div> <div class="chat-area"> <div class="chat-container"> <div class="chat-header"> <button class="fa fa-times close-chat"></button> </div> <div class="from"> <div class="label"> <label for="email_addr">From</label> </div> <input type="email" name="email_addr" id="email_addr" placeholder="example@mail.com"> </div> <div class="text"> <div class="label"> <label for="message">Message</label> </div> <textarea name="" placeholder="type your message" id="message"></textarea> </div> <div class="send_button"> <button id="send_button" class=""> Send <span class="fa fa-angle-right"></span> </button> </div> </div> </div> </div>');