var transportation = [

    {
        "url": "sepeda-pakde-jali",
        "nama": "Sewa Sepeda Pakde Jali",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "ontel.jpg",
        "rating":"3",
        "harga":"100.000",
        "jarak":"10 Menit ke Geopark ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    }
   ,{
        "url": "sejahtera-rental",
        "nama": "Sejahtera Rental",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "mobil.jpg",
        "rating":"4",
        "harga":"100.000",
        "jarak":"20 Menit ke Palabuhan Ratu",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    }
    ,{
        "url": "delman-pak-untung",
        "nama": "Delman Pak Untung",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "delman.jpg",
        "rating":"4",
        "harga":"100.000",
        "jarak":"20 Menit ke Geopark ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    }
    ,{
        "url": "jasa-becak-setempat",
        "nama": "Jasa Becak Setempat",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "becak.jpg",
        "rating":"3",
        "harga":"100.000",
        "jarak":"20 Menit ke Geopark ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    }
    ,{
        "url": "ojek-pangkalan",
        "nama": "Ojek Pangkalan",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "ojek.jpg",
        "rating":"4",
        "harga":"100.000",
        "jarak":"30km dari Cikaso",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    },{
        "url": "delman-pak-sobar",
        "nama": "Delman Pak Sobar",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "delman-3.jpg",
        "rating":"4",
        "harga":"100.000",
        "jarak":"20 Menit ke Geopark ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    },{
        "url": "sepeda-ontel-rent",
        "nama": "Fik Onthel Rent",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "ontel-1.jpg",
        "rating":"3",
        "harga":"100.000",
        "jarak":"10 Menit ke Gunung Gede ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    },{
        "url": "delman-vacatio",
        "nama": "Antar Jemput Delman",
        "lokasi": "Sukabumi, Jabar",
        "gambar": "delman-3.jpg",
        "rating":"3",
        "harga":"100.000",
        "jarak":"13 Menit ke Geopark ",
        "deskripsi": "layanan antar ke Geopark  tujuan untuk membantu perekonomian warga setempat"
    }
    
    ]

