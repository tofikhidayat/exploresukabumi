var news_list = [

    {
        "url": "upacara-adat-ciptagelar",
        "kategory": "Upacara adat",
        "title": "Upacara adat Kampung Ciptagelar",
        "nama": "Upacara seren taun, bukti syukur masyarakat di cipta gelar",
        "lokasi": "kampung ciptagelar",
        "tanggal": "07/april/2018",
        "gambar": "destinations/ciptagelar.jpg",
        "oleh": "admin",
        "rating":"5",
        "credit": "sukabumiupdate.com",
        "deskripsi": "<p>Acara Seren Taun terbuka untuk pengunjung dari luar wilayah Kasepuhan Ciptagelar. Masyarakat Kasepuhan Ciptagelar konsisten menjalankan tatanan leluhur, terutama sistem pertanian. “Hanya padi titipan leluhur, 160 jenis varietas asli, yang ditanam di Kasepuhan.” ucap Yoyo Yogasmana, humas Kasepuhan Ciptagelar,</p><br><p>Menyambut Seren Taun, tujuh panggung disekitar Imah Gede menyajikan berbagai hiburan dari yang buhun / lama mulai dari seni jipeng / tanji dan topeng, ujungan, pantun buhun, dogdog lojor / alat musik pukul semacam gendang panjang dan angklung, gondang, seni topeng, hingga kesenian yang umum kita lihat seperti pencak silat, debus, pertunjukan wayang golek dan berbagai tarian. Uniknya, tiap panggung dilengkapi sistem pengeras suara yang hampir sama kuat, seolah berlomba merebut perhatian pengunjung.</p> <p>Puncak acara ini adalah ngadieuken pare, memasukkan sepocong pare indung / induk padi dalam Leuit Si Jimat, dilakukan secara simbolis oleh Pemimpin Kasepuhan Ciptagelar, Abah Ugi Sugriana Rakasiwi dan istrinya, Emak Alit.</p><br><p>Berkat kearifan lokal yang sudah dipertahankan selama ratusan tahun, masyarakat Kasepuhan Ciptagelar tidak pernah mengalami gagal panen, bahkan berlimpah dan menambah jumlah leuit tiap tahunnya. Tertarik untuk ikut serta dalam perayaan ini? Seren Taun ke 649 di Kasepuhan Ciptagelar akan dilaksanakan pada tanggal 15-17 September 2017 (tahin lala).</p>"
    },
    {
        "url": "penyu-menetas-ujung-genteng",
        "kategory": "Pantai",
        "title": "Penetasan penyu",
        "nama": "Melihat anak penyu menetas di Ujung Genteng",
        "lokasi": "Ujung genteng",
        "tanggal": "07/april/2018",
        "gambar": "galery/galery-7.png",
        "oleh": "admin",
        "rating":"4",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Pantai Pangumbahan adalah salah satu pantai yang banyak dikunjungi di Kawasan wisata Ujung Genteng. Pemandangan indah kolaborasi birunya air laut dengan hamparan pasir putih memanjang menjadi daya tarik tersendiri bagi pantai yang juga menjadi favorit para peselancar karena memiliki ombak setinggi dua sampai tiga meter.</p><br><p>Selain itu, yang membuat anti mainstream adalah adanya penangkaran penyu hijau di daerah pantai pangumbahan ini. Konservasi penyu ini dibawahi oleh kementrian kelautan dan perikanan dan buka siang maupun malam. Disini pengunjung bisa melihat bagaimana telur – telur penyu yang baru ditetaskan oleh induknya di tangkarkan untuk suatu saat tukiknya dilepaskan di laut Lenas.</p><br><p>Salah satu atraksi yang paling ditunggu oleh pengunjung adalah melihat secara langsung penyu bertelur. Biasanya penyu tersebut akan naik ke daratan pantai untuk bertelur dan dilakukan malam hari saat suasana sepi dan sunyi. Oleh karena itu, pengunjung dihimbau untuk tidak menimbulkan kegaduhan apapun saat penyu tersebut mulai prosesi bertelurnya. Jikalau penyu tersebut merasa terganggu maka penyu tersebut akan berhenti dan kembali ke laut lepas.</p><br><p>Untuk melihat penyu ini bertelur cukup membawa peralatan sederhana seperti senter dan baju hangat karena angin dari laut cukup kencang. Pengunjung biasanya akan berjalan berombongan dengan satu pemandu yang akan menjelaskan proses demi proses. Telur penyu yang dihasilkan mencapai ratusan butir dan segera dilidungi oleh petugas dari pencurian maupun predator. Setelah menetas dan menjadi tukik, proses selanjutnya adalah melepaskan mereka kembali ke habitat aslinya dilaut.</p>"
    },

    {
        "url": "tempat-selancar-sukabumi",
        "kategory": "pantai",
        "title": "Pempat berselancar di sukabumi",
        "nama": "Inilah beberapa Spot Selancar yang ada di Sukabumi",
        "lokasi": "cimaja",
        "tanggal": "07/april/2018",
        "gambar": "destinations/pantai-cimaja.jpg",
        "oleh": "admin",
        "rating":"5",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Area Palabuhanratu pada akhir pekan menjadi tujuan berselancar favorit bagi ekspatriat dan semakin banyak peselancar Indonesia yang berbasis di Jakarta karena kemudahan aksesibilitas semata. Sebagian besar ombak menghantam pantaidan membentuk dinding air besar. Kebanyakan peselancar melewatkan hal-hal lain menuju beberapa kilometer ke barat ke penangkapan ikan yang sepi dandesa sawah dikenal sebagai Cimaja, atau Pantai Cimaja.</p> <br><p>Cimaja tepat dikarang berbatu dicapai dengan berjalan kaki sekitar 300 meter dari jalan utama melalui sawah di tepi pantai. Tempat ini secara konsisten mampu untuk berselancar karena memiliki dasar batuan padat dan sudut sempurna selaras dengan arus naik selatan dan barat yang membengkak dari Samudera Hindia. Ombak menghempas di depanbatu koral dan batu berserakan di pantai seperti serangkaian alat musik raksasa setiap kali gelombang menggulung.</p>"
    }

    , {
        "url": "liburan-cisolok",
        "kategory": "Liburan",
        "title": "Liburan di Cisolok",
        "nama": "Libur Panjang, Jumlah Wisatawan Geyser Cisolok Sukabumi Meningkat",
        "lokasi": "Cisolok",
        "tanggal": "07/april/2018",
        "gambar": "destinations/cipanas-cisolok.jpg",
        "oleh": "admin",
        "rating":"4",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Libur panjang akhir pekan dan Paskah 2018, kunjungan wisatawan ke lokasi wisata Geyser Cisolok, di Desa Wangunsari, Kecamatan Cisolok, Kabupaten Sukabumi meningkat. Selama tiga hari ini, jumlah pengunjung mencapai lebih dari seribu orang.</p> <p>Sejak Jumat sampai hari Minggu ini, sekitar 500 kendaraan masuk ke Objek Wisata Geyser Cisolok ini, Ujar Febi Marwan, staff pemungutan retribusi objek wisata Geyser Cisolok ditemui sukabumiupdate.com, Minggu (1/4/2018).</p> <p>Para wisatawan datang menggunakan kendaraan roda dua maupun roda empat. Ada yang dari sekitar Sukabumi, namun banyak juga yang berasal dari luar daerah. Total kunjungan wisatawan pada akhir pekan kali ini lebih tinggi dibanding biasanya. Ada sekitar 600 orang yang mandi di kolam pemandian, serta lebih dari 1.000 orang bermain di area pemandian air panas alami</p> <p>Rizki Febriansyah (36 tahun) seorang pengunjung asal Kampung Buniwangi, Kecamatan Palabuhanratu sengaja datang ke Geyser Cisolok untuk memanfaatkan hari libur bersama keluarga. Lokasinya dinilai cocok untuk menghilangkan penat</p>"
    }


    , {
        "url": "gunung-gede-pangrango",
        "kategory": "Liburan",
        "title": "Pendakian Gunung Gede Pangrango Kembali Dibuka",
        "nama": "Pendakian Gunung Gede Pangrango Kembali Dibuka",
        "lokasi": "pangrango",
        "tanggal": "07/april/2018",
        "gambar": "destinations/gunung-gede.jpg",
        "oleh": "admin",
        "rating":"3",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Kegiatan pendakian di Gunung Gede Pangrango dibuka kembali pada 1 April mendatang. Pendakian sebelumnya ditutup sejak 1 januari 2018. Kepala Bidang Teknis Balai Taman Nasional Gunung Gede Pangrango (TNGGP) Mimi Murdiah mengatakan selain dibuka kembali kini pendakian menggunakan sistem yang baru yaitu pengambilan Surat Izin masuk Kawasan Konservasi (SIMAKSI) bisa dilakukan di semua pintu masuk pendakian. </p><br><p>Untuk mempermudah calon pendaki, Kalau bookingnya kan online. Kalau dulu ngambil SIMAKSI itu harus di Balai Besar TNGGP Cibodas. Kasian keburu cape bolak balik ngambil SIMAKSI. Kalau sekarang, mau naik dimana di pintu masuk itu dapat mengambil SIMAKSInya, ujar Mimi kepada sukabumiupdate.com, Kamis (22/3/2018).</p><br><p>Untuk mengurangi resiko kecelakaan dalam pendakian, kata Mimi, pendaki yang akan naik gunung diwajibkan memeriksakan dulu kesehatannya di setiap pintu masuk. Tidak bisa sekedar membawa surat keterangan sehat terkini dari dokter.</p><br><p>Sekarang kita bekerjasama dengan koperasi dengan dokter layanan kesehatan di setiap pintu masuk. Pada saat akan naik calon pendaki akan diperiksa dokter tentang saturasi, cek darah dan yang mempunyai riwayat penyakit diwajibkan membawa obat, jelasnya.</p><br><p>Mimi mengungkapkan, apabila calon pendaki tidak lolos cek kesehatan maka tidak bisa melakukan pendakian meskipun sudah mempunyai SIMAKSI. Jika tetap memaksa kami tidak akan bertanggung jawab jika sesuatu terjadi. Dan untuk pendaki perempuan yang sedang datang bulan itu tidak bisa naik karena kondiai fisiknya cenderung menurun serta untuk mengurangi angka kecelakaan</p>"
    }

    , {
        "url": "rute-baru-geopark",
        "kategory": "Liburan",
        "title": "rute ke geopark",
        "nama": "Rute Baru Menuju Geopark Ciletuh Lebih Pendek dan Indah",
        "lokasi": "ciletuh",
        "tanggal": "07/april/2018",
        "gambar": "cikanteh1.jpg",
        "oleh": "admin",
        "rating":"4",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Bagi yang ingin berwisata ke Geopark Ciletuh, Sukabumi, ini saatnya menjajal jalur baru. Jalur yang baru lebih cepat dan keren view-nya,â€ ujar Hendrayadi, seorang pengunjung Ciletuh yang melewati jalur baru, Selasa, 16 Januari 2018. Saya bisa menikmati pemandangan pantai dari atas bukit.</p><br><p>Warga Bogor ini mengatakan hanya butuh waktu sekitar 2 jam 30 menit untuk sampai di Ciletuh. Kalau lewat jalur lama, bisa sampai 4 jam, kata Hendrayadi. Sebelumnya, beredar video yang menunjukkan rute baru menuju Geopark Ciletuh. Video pendek itu diunggah akun Instagram @pemprovjabargoid awal Januari lalu. Video ini sempat menjadi trending topic. </p><br><p>Dalam video itu disebutkan jalur baru sepanjang 33 kilometer terbentang mulai Simpang Loji di Jalan Pelabuhan Ratu hingga melewati Muara Cisaar di Kecamatan Ciemas. Ketika sampai di Puncak Darma, selanjutnya menuju Palangpang, yang merupakan pusat Geopark Ciletuh. Rute baru ini mengambil jalur tepi pantai.</p><br><p>Sedangkan jalur lama berjarak 71 kilometer, bermula di Simpang Loji, berlanjut ke Kiara Dua, Simpang Waluran, Malereng, Taman Jaya, hingga Palangpang. Jadi rute baru ini memangkas jarak hingga lebih dari separuhnya. Pembangunan akses baru menuju Geopark Ciletuh dimulai pada awal 2017 dan menelan biaya Rp 200 miliar. Di sepanjang jalur baru ini, wisatawan akan menikmati keindahan taman bumi berusia jutaan tahun.</p><br><p>Geopark Ciletuh merupakan pengembangan kawasan dengan memasukkan keragaman geologi, hayati, dan budaya melalui konservasi dan rencana tata ruang wilayah. Di Ciletuh, pengunjung bisa menikmati berbagai sajian alam, mulai air terjun, bentang alam, batuan unik, gua laut, pantai, serta geyser. Di sini juga terdapat cagar alam dan konservasi penyu hijau.</p>"
    }

    , {
        "url": "puncak-darma",
        "kategory": "Pantai",
        "title": "Puncak dharma",
        "nama": "Puncak Darma Kabupaten Sukabumi Makin Banyak Dikunjungi Wisatawan",
        "lokasi": "girimukti",
        "tanggal": "07/april/2018",
        "gambar": "destinations/puncak darma.png",
        "oleh": "admin",
        "rating":"3",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Puncak Darma. Objek wisata yang berada di Kecamatan Ciemas, Kabupaten Sukabumi ini makin banyak dikunjungi wisatawan. Destinasi yang berada di kawasan Geopark Nasional Ciletuh Palabuhanratu ini menjadi tempat favorit pengunjung dari luar daerah.</p><br><p>Setiap Sabtu dan Minggu, libur akhir pekan pasti ramai. Semakin kesini semakin ramai, ujar Badrus Salam (29 tahun) salah satu petugas akses masuk di Puncak Darma. </p><br><p>Tidak hanya dari Sukabumi atau kota-kota tetangga, para pengunjung juga datang dari sejumlah daerah provinsi lain. Bahkan turis asing. Badru menambahkan, pengunjung pada akhir pekan bisa mencapai lebih dari 1.000 orang. Para wisatawan didominasi anak muda.</p><br><p>Dari Jakarta, Bogor, Tangerang, dari Aceh juga ada. Pernah juga ada rombongan dari Malaysia, jelas Badru. Sementara itu Dinar Sulaiman (33 tahun) pengunjung asal Leuwiliang, Bogor mengatakan, dirinya beserta rekan kerja sengaja datang ke puncak Darma karena penasaran dari banyak pemberitaan. </p> <p>Hasilnya, Ia rasakan sendiri bahwa objek Wisata Puncak Darma memang mempesona. Terbayar perjalan jauh saya. Pemandangan disini sangat indah, mempesona, dan bikin tenang. Enggak nyesel pokonya main kesini, ujar Dinar. </p>"
    }

    , {
        "url": "sunset-di-palangpang",
        "kategory": "Pantai",
        "title": "perburuan sunset",
        "nama": "Yuk Berburu Sunset di Pantai Palangpang Sukabumi, Dijamin Nggak Nyesel",
        "lokasi": "Palangpang",
        "tanggal": "07/april/2018",
        "gambar": "destinations/pantai-palangpang.jpg",
        "oleh": "admin",
        "rating":"5",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Bentangan pantai di wilayah Selatan Kabupaten Sukabumi menyajikan keindahan yang seakan tiada duanya. Diantaranya Pantai Palangpang yang berada di Desa Ciwaru, Kecamatan Ciemas, Kabupaten Sukabumi. </p><br><p>Pantai yang merupakan zona inti Geopark Ciletuh ini menjadi lokasi tujuan wisatawan lokal maupun luar kota. Selain lautnya yang tenang, pada waktu sore pemandangan di pantai ini akan semakin indah dengan terbenamnya matahari atau yang disebut sunset. </p><br><p>Biasanya menjelang sore, wisatawan akan berkumpul demi sunset. Semenjak jadi zona inti Geopark Ciletuh, Pantai Palangpang mengalami peningkatan kunjungan wisatawan baik lokal maupun dari luar kota. Mereka bisa berenang, berselfie dengan latar tebing puncak Darma bahkan berburu sunset, kata Kompepar Desa Ciwaru Yudi Mulyadi kepada sukabumiupdate.com, Minggu (11/3/2018). </p><br><p>Ia menuturkan, rata-rata perbulan wisatawan yang datang mencapai 8 ribu sampai 10 ribu. Tak adanya pungutan retribusi dan harga makanan juga oleh-oleh lainnya yang normal membuat pengunjung betah.</p> <p>Kami juga menekankan agar para pengunjung tetap menjaga kebersihan dengan membuang sampah pada tempatnya. Sedangkan untuk keamanan pengunjung yang mau berenang, ada tim Balawisata pos 16 Palangpang, pungkasnya</p>"
    }


    , {
        "url": "spot-foto-di-citepus",
        "kategory": "Pantai",
        "title": "spot foto menarik",
        "nama": "Inilah beberapa spot foto menarik di Pantai Citepus",
        "lokasi": "ciletuh",
        "tanggal": "07/april/2018",
        "gambar": "geopark-3.jpg",
        "oleh": "admin",
        "rating":"3",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Objek wisata pantai Citepus Istana Presiden memiliki lokasi yang pas untuk berfoto. Tepatnya di batu karang yang ada dikawasan pantai tersebut. Pengunjung hilir mudik bergantian menaiki batu karang untuk mengabadikan momen liburannya. Sebab dengan menaiki karang itu bentangan pantai dan laut yang luas bakal tertangkap jelas di foto. </p><br><p>Salah seorang pengunjung Siti Faujiah (19 tahun) mengatakan datang ke pantai tersebut untuk bermain bersama dengan teman-temanya sambil berfoto dan selfie diatas batu karang dengan pemandangan laut lepas. Indah kan selfie di atas batu dengan pemandangan birunya laut lepas, ujar warga Cimaja Kecamatan Cisolok ini. </p><br><p>Pengunjung lainnya, Rudiansyah (37 tahun) mengungkapkan sengaja datang bersama keluarganya dari Cipanas, Kabupaten Cianjur karena penasaran dengan keindahan objek wisata tersebut dari pemberitaan media.</p><br><p>Saat mengunjungi pantai tersebut Rudiansyah dan keluarganya merasa kagum dengan keindahan panorama alamnya. Sengaja kesini, penasaran dengan pemberitaan katanya tempatnya indah, ternyata dari kemarin saya kesini tak bosen. Balik lagi pastinya liburan nanti, ujarnya. </p>"
    }


    , {
        "url": "geopark-menjadi-ugg",
        "kategory": "Liburan",
        "title": "Pengukuhan geopark",
        "nama": "Bulan Depan Geopark Sukabumi Dikukuhkan Jadi UGG",
        "lokasi": "Ciletuh",
        "tanggal": "07/april/2018",
        "gambar": "destinations/curug sodong.jpg",
        "oleh": "admin",
        "rating":"4",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Geopark Ciletuh Palabuhanratu sudah dipastikan dikukuhkan menjadi UNESCO Global Geopark (UGG) pada bulan April mendatang. Karena ini adalah icon dunia jadi bulan April ini kita akan dikukuhkan menjadi UGG. Jadi sebenarnya saya sebetulnya harus ke Prancis, tapi Pak Gubernur meminta general manager saya yang berangkat karena kesibukan untuk posisi pemilihan kepala daerah, ujar Bupati Sukabumi Marwan Hamami dalam acara AHY Ngariung di Palabuhanratu, Rabu (21/3/2018) malam. </p><br><p>Marwan menambahkan, setelah itu pada September Geopark Ciletuh Palabuhanratu dikukuhkan di Milan, Italy. Marwan yakin dengan dikukuhkanya Geopark Ciletuh Palabuhanratu jadi UGG ini membawa semangat pembangunan di Kabupaten Sukabumi. Semangat untuk membangun Kabupaten Sukabumi, jelasnya. </p><br><p>Marwan menuturkan, Geopark Ciletuh Palabuhanratu memiliki keistimewaan tersendiri karena bebatuanya dari tektonik bukan dari vulkanik dan satu-satunya di Indonesia. Menurut dia, delapan kecamatan yang berada di wilayah pesisir masuk kawasan Geopark Ciletuh Palabuhanratu dari mulai Kecamatan Cisolok hingga Kecamatan Ciracap.</p><br><p>Geopark ini satu-satunya di Indonesia yang bebatuanya dari tektonik bukan dari vulkanik. Kalau di Bali itu dari vulkanik termasuk Danau Toba. Tapi kalau kita dari tubrukan dua benua Asia dan Australia, jelasnya.</p>"
    }

    , {
        "url": "pesona-batu-batik",
        "kategory": "Liburan",
        "title": "psona batu batik",
        "nama": "Pesona Batu Batik, Geosite di Zona Inti Geopark Ciletuh ",
        "lokasi": "ciletuh",
        "tanggal": "07/april/2018",
        "gambar": "destinations/buniayu.jpg",
        "oleh": "admin",
        "rating":"3",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p>Kawasan Geopark Ciletuh Palabuhanratu di selatan Sukabumi punya banyak spot dengan daya tarik pemandangan hamparan bebatuan berusia jutaan tahun. Salah satunya Batu Batik, geosite yang berada di zona inti geopark. </p><br><p>Mengunjungi batu batik bukan perkara mudah. Lokasinya yang berada di kawasan konservasi, Suaka Margasatwa Cikepuh, membuat aktivitas manusia di tempat itu harus dibatasi. Namun, bukan berarti pesona Batu Batik sama sekali tidak bisa dinikmati. Ada cara lain yang bisa dilakukan untuk menikmati keindahannya, tanpa menerobos aturan.</p><br><p>Pemandangan Batu Batik bisa dilihat dari laut lepas dengan menggunakan perahu nelayan. Kini sudah ada penyewaan perahu ke lokasi Batu Batik, dari Pantai Cikadal, Desa Mandrajaya, Kecamatan Ciemas. Waktu tempuhnya hanya sekitar 30 hingga 45 menit. Disebut batu batik karena memiliki motif seperti batik. Memang di sekitar lautan tersebut banyak yang unik terutama bentuk batu dan warnanya. Selain batu batik, juga ada yang disebut batu. </p><br><p>Punggung naga, ujar Agus (41) nelayan warga Kampung Neglasari RT 2 RW 2 Desa Mandrajaya ditemui sukabumiupdate.com, Jumat (2/3/2018). Karakteristik paling menonjol dari Batu Batik adalah warnanya yang coklat kemerahan. Selain Batu Batik pengunjung juga bisa melihat pemandangan lain, seperti kawasan Hutan Batu Nunggul, Cikepuh, Legon Jawa, dan Sodong Parat. </p>"
    }




    , {
        "url": "pesona-alami-dharma-ciletuh",
        "kategory": "Liburan",
        "title": "pesona puncak dharma",
        "nama": "Pesona Alami dari Ketinggian Puncak Darma Geopark",
        "lokasi": "ciletuh",
        "tanggal": "07/april/2018",
        "gambar": "destinations/puncak darma.png",
        "oleh": "admin",
        "rating":"3",
        "credit": "SUKABUMIUPDATE.com",
        "deskripsi": "<p> Belum lengkap rasanya jika berkunjung ke Geopark Nasional Ciletuh - palabuhanratu namun tidak menginjakan kaki di Puncak Darma. Tempat ini menjadi salah satu lokasi favorit para wisatawan.<br>Puncak Darma terletak di Kampung Pasirmuncang RT 2 RW 6, Desa Girimukti, Kecamatan Ciemas, Kabupaten Sukabumi. Aksesnya semakin mudah ditempuh setelah akses jalan Loji-Puncak Darma dibangun.</p><br><p>Dari ketinggian Puncak Darma, wisatawan disuguhkan pemandangan alam yang menakjubkan. Terlihat jelas pesawahan, bukit, aliran sungai yang mengalir ke laut sektiar Pantai Palangpang. Tentu, lokasi ini sangat cocok bagi pengunjung untuk berswafoto.</p><br><p> Di tempat ini, sudah tersedia area parkir yang cukup luas. Ada pula warung - warung milik warga yang menyediakan jajanan harga terjangkau. Sangat luar biasa. Perjalanan kami yang sangat jauh, dan rasa lelah langsung terobati dengan melihat keindahan di Puncak Darma, kata Neng Intan, seorang pengunjung.</p>"
    }

];