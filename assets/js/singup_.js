
$(".show-hide-password").click(function(event) {
  if ($(this).attr('show') == "false") {
   $(".show-hide-password").removeClass('fa-eye').addClass('fa-eye-slash');    
   $(".show-hide-password").attr('show', 'true');
   $(".password-secure").attr('type', 'text');
  }
  else{
   $(".show-hide-password").removeClass('fa-eye-slash').addClass('fa-eye');    
   $(".show-hide-password").attr('show', 'false'); 
   $(".password-secure").attr('type', 'password');
  }

});





function alert_error(string){
    swal({
             title: 'ooops !',
             text: string,
             type: 'error',
             confirmButtonColor: '#3085d6',
             confirmButtonText: 'Ok'
           });
     endload();
}


           $("#password").passwordStrength();

           $("#password").keyup(function(event) {
               score = $(this).attr('score');
               if (score >= 25) {
                   $(this).css('borderColor', '#5CCF04');
               } else if (score >= 18) {
                   $(this).css('borderColor', '#79E226');
               } else if (score >= 13) {
                   $(this).css('borderColor', '#FFE52B');
               } else if (score >= 7) {
                   $(this).css('borderColor', '#FF4B2B');
               } else {
                   $(this).css('borderColor', '#FF2B2B');
               }
           });



var canuse = false;
 var error = "";

           function alert_register(string){
           swal({
             title: 'Email or Name has registered with same account !',
             text: "  Are you want to register again?",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             cancelButtonText:'No I want to Login',
             confirmButtonText: 'Yes I want'
           }).then((result) => {
             if (result.value) {
               swal(
                 'Deleted!',
                 'Your file has been deleted.',
                 'success'
               ) 
             }
             else if(result.dismiss === swal.DismissReason.cancel)
             {
               swal(
                 'Deleted!',
                 'Your file has been deleted.',
                 'error'
               )
             }
           })
           }




           $("#repassword").keyup(function(event) {
               if ($(this).val() != $("#password").val()) {
                   $(this).css('borderColor', '#FF2B2B');
               } else {
                   $(this).css('borderColor', '#5CCF04');
               }
           });

           $(".text").keyup(function() {

               if ($(this).val().length < 5) {
                   $(this).css('borderColor', '#FF2B2B');
               } else {
                   $(this).css('borderColor', '#fff');
               }
           });

           $(".profile_selector").change(function() {
               if ($(this).val() != "") {

               }
               $(this).closest('label').find('.camera').css('opacity', '0');
           });







           $("input[type=file]").change(function(event) {
loading();
               if ($("input[type=file]").val() == "") {
                   $("input[type=file]").closest('label').css('borderColor', '#FF2B2B');
                   $(".profile").attr('src', '');
                   $('.camera').css('opacity', '1');
               } else {
                
                   $(this).closest('label').css('borderColor', 'transparent');
                  
            var input = $("input[type=file]")[0].files[0];
              filedata =  new FormData();
              filedata.append('file',input);

              $.ajax({
                url: '../config/send_file_register.php',
                type: 'POST',
                data: filedata,
                contentType:false,
                processData:false,
                success:function(result)
                {
                  console.log(result);
                  if (result == "size_error") {
                    alert_error('the file you uploaded is too large please upload the file below 2 mb');
                  }
                  else  if (result == "format_error") {
                    alert_error('please upload the image with the format png.svg, jpg, jpeg or gif');
                  }
                  else
                  {
                    var file_location = result;
                    var file = file_location.split("../").pop();
                    var file_data = file.split("/").pop();
                   
                      $(".profile").attr('src','../'+file);
                      endload();
                     
                  }

                }});
               }


           });




           $(document).on('click', '.submit', function(event) {
               event.preventDefault();
               loading();
      


               var datasample = $(".singup-content input");
               for (var i = 1; i <= datasample.length; i++) {
                   var singdata = $(".singup-content .content-form:nth-child(" + i + ") .singup-form input").val();
                   if (singdata.length <= 5) {
                       var singdata = $(".singup-content .content-form:nth-child(" + i + ") .singup-form input").css('borderColor', '#FF2B2B');
                       if ($("input[type=file]").val() == "") {
                           $("input[type=file]").closest('label').css('borderColor', '#FF2B2B');
                           $(".profile").attr('src', '');
                           $('.camera').css('opacity', '1');
                       } else if (parseInt($("#password").attr('score')) < 18) {
                        $("#password").css('borderColor', '#FF2B2B');

                       } else {
                           $("input[type=file]").closest('label').css('borderColor', 'transparent');
                       }

                   } else if (parseInt($("#password").attr('score')) < 18) {
                       $("#password").css('borderColor', '#FF2B2B');
                   }

                   else if ($("#repassword").val() != $("#password").val()){
                    $("#repassword").css('borderColor', '#FF2B2B');
                   } 

                   else {
                       var singdata = $(".singup-content .content-form:nth-child(" + i + ") .singup-form input").css('borderColor', '#fff');
                       if ($("input[type=file]").val() == "") {
                           $("input[type=file]").closest('label').css('borderColor', '#FF2B2B');
                           $(".profile").attr('src', '');
                           $('.camera').css('opacity', '1');
                       } 
                        else {
                           $("input[type=file]").closest('label').css('borderColor', 'transparent');
                        //setting variael
                           var file_selector = $(".profile_selector");
                           var name = file_selector[0].files[0].name;
                           var type = name.split(".").pop();
                           var size = file_selector[0].files[0].size;

                          
                           if (type != "png" && type != "PNG" && type != "jpg" && type != "jpg" && type != "jpeg" && type != "svg" && type != "SVG" && type != "JPEG" && type != "gif" && type != "GIF") {
                               var canuse = false;
                               var error = "the image you upload is not supported please upload the image with format png, svg or jpg";
                           } else {
                               if (size > 2000000) {
                                   var canuse = false;
                                   var error = "the image you upload is too big please upload the image below 2mb";
                               } else {
                                   var canuse = true;
                               }
                           }
                       }
                   }


               }

               if (canuse != true) {
                if (error == "") {
                   alert_error(error);
                   }   
                else
                  {alert_error("please complete the field");}

               }
               else if ( $(".profile").attr('src') == "") {
                   $("input[type=file]").closest('label').css('borderColor', '#FF2B2B');
               }
               else{
                send_data(type);
               }

               endload();
               return false;

           });










          function send_data(type)
           {
            var img_profile = $(".profile").attr('src').split("/").pop();

         
            var email = $("#email").val();
            var name = $("#username").val();
            var password = $("#repassword").val();

            $.ajax({
          url: '../config/new_user.php',
          type: 'POST',
          data: {
          email:email,
          name:name,
          password:password,
          file_type:img_profile,
          },
          success:function(result)
        {
          //sukses

          if (result == 'already') {
              swal({
             title: 'Ooops !',
             text: 'Account with this email or name already registered you should try by email or another name !',
             type: 'error',
             confirmButtonColor: '#3085d6',
             confirmButtonText: 'Ok'
           })
          }
          else
          {
            swal({
                 title: 'Congratulation ',
                 text: 'You have successfully registered if you want to login now ?',
                 type: 'success',
                 confirmButtonColor: '#3085d6',
                 confirmButtonText: 'Ok'
           }).then((result) => {
          if (result.value) {
            window.location = "login.php";
          }
        })
           
          }
          
        }
        })

           }

           endload();