<?php
require("require.php");
$filename = $_FILES['file']['name'];
$file_id  = date("Ymdh_") . $filename;
$type     = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
$location = "../assets/image/post_images/" . $file_id;
if ($filename == "") {
    echo "nofile";
} else {
    
    if ($_FILES['file']['size'] > 5000000) {
        print("size_error");
    } else {
        if ($type != "png" && $type != "jpg" && $type != "gif" && $type != "svg") {
            print("format_error");
        } else {
            
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                echo $file_id." success";
                
            } else {
                echo "error";
            }
        }
        
    }
    
}


?>
