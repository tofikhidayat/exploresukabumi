<?php
require ("secure_login.php");
$type = $_GET['post_type'];
$post_id = $_GET['post_id'];
$show = $lib->quick_priview($post_id);
$data = $show->fetch(PDO::FETCH_OBJ);
$user_id = $data ->by_id;


?>

<div id="destination_temp">

	 <link rel="stylesheet" type="text/css" href="../assets/css/main.style.css">
     <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
      <link href="../assets/css/owl.carousel.css" rel="stylesheet">
      <link href="../assets/css/destination.css" rel="stylesheet">
      <link href="../assets/css/galery.css" rel="stylesheet">
      <link href="../assets/css/destination-data.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
<div class="search-unres inactive">
      <div class="search-result ">
         <div class="container">
            <div class="research">
               <div class="search-title"><span>Cari destinasi , dan Artikel lainya</span></div>
               <div class="input-search">
                  <div class="ci">
                     <input type="text" name="" placeholder="Cari disini">
                     <button type="button"><i class="fa fa-search"></i></button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="destination-data  dest-data-show pb-0 " style="padding-top: 0px !important">
      <div class="container pb-20">
          <div class="section-mode">
            <div class="image-page">
               <img src="../assets/image/post_images/<?php   print($data->post_images)?>" id="image" class="list-galery-data pointer">
            </div>
             <p>
                <div class="title pl-0" style="left: 10px;">
                     <h1 class="text-left fs-30 uppercase" id="title-dest"></h1>
                  
                    <div class="py-7" style="left: -10px;">
                        <span id="location" class="fs-15"><strong><span class="fs-25 px-10 pl-0 fa fa-map-marker"></span> Jawa Barat</strong> </span>

                    </div>

                      

                  </div>
                <div class="pt-10 px-10  pr-20 text-data" >
                    <p id="description" class="" style="line-height: 1.6; color: #666 !important;">
                    	<div>
                    		<?php   print($data->post_content)?>
                    	</div>
                    </p>
                </div>
          </div>
      </div>
   </div>
   <div class="share-area py-10">
      <div class="container">
         <div class="orvfo share-me">
            <button type="button" class="capital share-button-to share-me-please"> <i class="fa fa-share-alt fs-25"></i> <span class="pl-10 fs-15" style="top:-3px">bagikan</span></button>
          
         </div>

         <div class="orvfo rate-me">
            <button type="button" class=" capital like-me" liked="false" ><i class="fs-25"></i><span class="pl-10 fs-15 " style="top:-3px">sukai</span></button>
         </div>


         <div class="orvfo rating-me hiden">
            <div class="rate-area pb-40" style="left: -10px;">
                  <div class='rate' >
                    <div class="sub-rate" rating=''>
                        <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
         </div>
      </div>
   </div>

   <footer class="footer">
      <div class="container">
         <div class="foot-logo ">
            <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.html" title=""><img src="../assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
            <div class="some">
               <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
               <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3>
            </div>
         </div>
         <div class="foot-nav">
            <div class="nav-title mb-20">
            </div>
            <div class="nav-body">
               <nav>
                  <ul>
                     <li>
                        <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.html" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.html" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.html" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.html" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.html" title="news">transportasi</a> </li>
                        </ul>
                     </li>
                     <li>
                        <ul>
                           <li class="capital"><span>hubungi kami</span></li>
                           <li>
                              <p>Telepon</p>
                           </li>
                           <li>
                              <p>+123 456 789 </p>
                           </li>
                           <li>
                              <p>Email</p>
                           </li>
                           <li>
                              <p>eksukabumi@hotmail.com</p>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <ul>
                           <li class="capital"><span>sosial media</span></li>
                           <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                           <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                           <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                           <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                        </ul>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
      </div>
   </footer>
   <div class="con-copy">
      <input type="text" class="" name="" id="value-link">
   </div>

   <div class="search-popup inactive">
      <button type="button" class="fa fa-close-o dismis-c"></button>
      <div class="container-v">
         <div class="filter">
            <div class="search">
               <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
               <button type="button" class="fa fa-close-o remove-text"></button>
               <button type="button" class="fa fa-to-go button-w"></button>
            </div>
         </div>
         <div class="filter-data ">
            <div class="pt-20 pb-10">
               <span class="fs-14 c-black ">Filter</span>
            </div>
            <div class="filter-content">
               <ul>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Semua</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Destinasi</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Berita</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Akomondasi</span></label>
                  </li>
                  <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                     <span class="to-data px-10">Hotel</span></label>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="ads-popup-hotel inactive">
      <div class="ads-container">
         <div class="ads-image">
            <img src="" alt="">
         </div>
         <div class="ads-content">
            <h3 class="oswald fs-18 uppercase pb-10 hotel-name">hotel</h3>
            <ul class="list-none ">
               <li>
                  <p class="fs-14 hotel-desription"></p>
               </li>
               <li class="py-5 pt-10 list-none py-5">
                  <h6> <i class="fa fa-cutlery fs-17"></i><span class="fs-14 pl-10">gratis makan</span> </h6>
               </li>
               <li class="py-4 list-none py-5">
                  <h6> <i class="fa fa-map-marker fs-17"></i><span class="fs-14 pl-10 jarak"></span> </h6>
               </li>
               <li class="py-5 parker">
                  <strong class="parking oswald fs-11">
                     <p>p</p>
                  </strong>
                  <span class="pl-10 fs-14">Parkir</span>
               </li>
            </ul>
            <div class="button-chart">
               <span class="chart">
                  <p class="fs-14 capital">mulai dari</p>
                  <p class="fs-14 ">Rp.<span class="rp harga"></span>/malam</p>
               </span>
               <button type="button" class="py-7 fs-14 px-10 capital book " dismis-hotel>pesan sekarang</button>
            </div>
         </div>
         <button type="" dismis-hotel class="dismis  closed fa fa-times fs-17"> </button>
      </div>
      <div class="popup-closer-area"></div>
   </div>
   <div class="ads-popup-rental inactive">
      <div class="container-ads">
         <div class="area-title pt-15 pb-">
            <h3 class="fs-18 text-left capital rental-name"  ></h3>
         </div>
         <div class="area-list">
            <ul class="list-none">
               <li class="phone"><a href="" title=""><i class="fa fa-phone fs-25 w-25 mr-20 py-10 " aria-hidden="true" style="top: 5px;"></i> <span></span></a></li>
               <li class="twitter"><a href="" title=""><i class="fa fa-twitter fs-25 w-25 mr-20 py-10 " aria-hidden="true" style="top: 5px;"></i> <span></span></a></li>
               <li class="facebook"><a href="" title=""><i class="fa fa-facebook fs-25 w-25 mr-25 py-10 " aria-hidden="true" style="top: 5px;"></i><span class="pl-8"></span></a></li>
            </ul>
         </div>
         <div class="area-button pt-30 pb-10">
            <button type="button" class="fs-18 fa fa-times" dismis="rental"></button>
         </div>
      </div>
      <div class="area-closer"></div>
   </div>



   <div class="share-popup in inactive">
     <div class="share-container">
       <div class="share-area">
       <div class="share-title pb-15">
         <h3 class="fs-20 oswald text-left capital">bagikan ke</h3>
       </div>
       <div class="share-content">
         <ul class="list-none">
           <li><button type="" class="share-to to-fb fs-14 capital" to="fb"><i class="fs-18 fa fa-facebook"></i> facebook </button></li>
           <li><button type="" class="share-to to-tw fs-14 capital" to="tw"><i class="fs-18 fa fa-twitter"></i> twitter </button></li>
           <li><button type="" class="share-to to-ig fs-14 capital" to="ig"><i class="fs-18 fa fa-instagram"></i> instagram </button></li>
           <li><button type="" class="share-to to-gp fs-14 capital" to="gp"><i class="fs-18 fa fa-google-plus  "></i> google plus </button></li>
           <li><button type="" class="share-to to-url fs-14 capital" to="url"><i class="fs-18 fa fa-link"></i> salin url </button></li>
         </ul>
       </div> 
       <div class="buttton-close">
         <button type="button" onclick="close_share()" class="fa fa-times fs-18"></button>
       </div>  
       </div>
     </div>
     <div class="close-share"></div>
   </div>

   <div class="popup">
      <div class="content-popup">
         <button type="button" class="dismis"><i class="fa fa-times"></i></button>
         <video src="" id="video-play" autoplay="" controls=""></video>
      </div>
      <div class="area-close"></div>
   </div>
   <div class="popup-rad ">
      <div class="image"><img src="../assets/image/galery/galery-12.png" alt="">
         <button type="button" close="popup"><i class="fa fa-close"></i></button>
      </div>
      <div class="area-close"></div>
   </div>

<script src="../assets/js/jquery.js"></script>
      <script src="../assets/js/owl.carousel.js"></script>
      <script src="../assets/js/jquery.auto-complete.js"></script>
   <script src="../assets/js/action.js"></script>

</div>
</div>


<div id="news_temp">

    <link rel="stylesheet" type="text/css" href="../assets/css/main.style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="../assets/css/owl.carousel.css" rel="stylesheet">
    <link href="../assets/css/destination.css" rel="stylesheet">
    <link href="../assets/css/galery.css" rel="stylesheet">
    <link href="../assets/css/news.css" rel="stylesheet">
    <link href="../assets/css/news-new.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
     
</head>

<div class="container">
    <div class="page-content-parent">
        <div class="parent-data">
            <div class="heading">
                <div class="intittle">
                    <h1 id="name"><?php   print($data->post_name)?></h1></div>
                <div class="sub-time">
                    <h6><div class="time"><i class="fa fa-clock-o fs-20"></i><span class="fs-14" id="time"><?php   print($data->date_modified)?></span></div><div class="time"><i class="fa fa-user-circle-o fs-20"></i><span class="fs-14" id="by">
                    	<?php
                    	$user = $lib->by($user_id);
                    	$username = $user->fetch(PDO::FETCH_OBJ);
                     	print($username->name);

                    	?>
                    		
                    	</span></div><div class="time"><i class="fa fa-paperclip fs-20"></i><span class="fs-14" id="type"><?php   print($data->post_type)?></span></div></h6></div>
                    <div class="rate-area pb-40 hiden" >
                  <div class='rate' style="left: 5px; top: -5px;">
                    <div class="sub-rate" rating=''>
                        <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="page-content-m">
                <div class=" image-page "><img src="../assets/image/post_images/<?php   print($data->post_images)?>" alt="" id="news-image">
                    <div class="credit-tag">
                        <h6>Credits By : <span id="credit"></span> </h6></div>
                </div>
                <div class="page-desc-m">
                    <p id="page-descript" class="fs-16 " style="color: #666;">
                    	<?php   print($data->post_content)?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="category">
        <div class="search">
            <input type="text" name="" class="search-news" placeholder="Cari Berita">
            <button type="button" class=""> <i class="fa fa-search"></i></button>
        </div>
        <div class="category-title">
            <h3 class="fs-18">Kategori</h3></div>
        <div class="list-category">
            <ul>
                <li><span>Air terjun</span></li>
                <li><span>Pantai</span></li>
                <li><span>liburan</span></li>
                <li><span>Upacara Adat</span></li>
            </ul>
        </div>
        <div class="terkait">
            <div class="category-title">
                <h3 class="fs-18">Berita Terkait</h3></div>
            <div id="list-data"></div>
        </div>
    </div>
</div>
</div>
   <div class="share-area py-10">
      <div class="container">
         <div class="orvfo share-me">
            <button type="button" class="capital share-button-to share-me-please"> <i class="fa fa-share-alt fs-25"></i> <span class="pl-10 fs-15" style="top:-3px">bagikan</span></button>
          
         </div>

         <div class="orvfo rate-me">
            <button type="button" class=" capital like-me" liked="false" ><i class="fs-25"></i><span class="pl-10 fs-15 " style="top:-3px">sukai</span></button>
         </div>
      </div>
   </div>


        <footer class="footer">
            <div class="container">
                <div class="foot-logo ">
                    <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.html" title=""><img src="../assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
                    <div class="some">
                        <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
                        <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3></div>
                </div>
                <div class="foot-nav">
                    <div class="nav-title mb-20">
                    
                    </div>
                    <div class="nav-body">
                        <nav>
                           <ul>
                               <li>
                                  <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.html" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.html" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.html" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.html" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.html" title="news">transportasi</a> </li>
                        </ul>
                               </li>
                                <li>
                                   <ul>
                                       <li class="capital"><span>hubungi kami</span></li>
                                       <li><p>Telepon</p></li>
                                       <li><p>+123 456 789 </p></li>
                                       <li><p>Email</p></li>
                                       <li><p>eksukabumi@hotmail.com</p></li>
                                      
                                   </ul>
                               </li>

                                <li>
                                   <ul>
                                       <li class="capital"><span>sosial media</span></li>
                                       <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                                        <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                                         <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                                          <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                                   </ul>
                               </li>
                           </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>

   <div class="share-popup in inactive">
     <div class="share-container">
       <div class="share-area">
       <div class="share-title pb-15">
         <h3 class="fs-20 oswald text-left capital">bagikan ke</h3>
       </div>
       <div class="share-content">
         <ul class="list-none">
           <li><button type="" class="share-to to-fb fs-14 capital" to="fb"><i class="fs-18 fa fa-facebook"></i> facebook </button></li>
           <li><button type="" class="share-to to-tw fs-14 capital" to="tw"><i class="fs-18 fa fa-twitter"></i> twitter </button></li>
           <li><button type="" class="share-to to-ig fs-14 capital" to="ig"><i class="fs-18 fa fa-instagram"></i> instagram </button></li>
           <li><button type="" class="share-to to-gp fs-14 capital" to="gp"><i class="fs-18 fa fa-google-plus  "></i> google plus </button></li>
           <li><button type="" class="share-to to-url fs-14 capital" to="url"><i class="fs-18 fa fa-link"></i> salin url </button></li>
         </ul>
       </div> 
       <div class="buttton-close">
         <button type="button" onclick="close_share()"></button>
       </div>  
       </div>
     </div>
     <div class="close-share"></div>
   </div>

       
        
        <div class="search-popup inactive">
            <button type="button" class="fa fa-close-o dismis-c"></button>
              <div class="container-v">
                    <div class="filter">
                    <div class="search">
                        
                        <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
                        <button type="button" class="fa fa-close-o remove-text"></button>
                        <button type="button" class="fa fa-to-go button-w"></button>
                       
                    </div>
                   
                </div>
                 <div class="filter-data ">
                        <div class="pt-20 pb-10">
                            <span class="fs-14 c-black ">Filter</span>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Semua</span></label>
                                </li> 
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Destinasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Berita</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Akomondasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Hotel</span></label>
                                </li>
                            </ul>
                        </div>
                    </div>
              </div>
        </div>

<div class="popup">
    <div class="content-popup">
        <button type="button" class="dismis"><i class="fa fa-times"></i></button>
        <video src="" id="video-play" autoplay="" controls=""></video>
    </div>
    <div class="area-close"></div>
</div>
<div class="popup-rad ">
    <div class="image"><img src="../assets/image/galery/galery-12.png" alt="">
        <button type="button" close="popup"><i class="fa fa-close"></i></button>
    </div>
    <div class="area-close"></div>
</div>
</body>

<script id="item-news-sel" type="text/x-jQuery-tmpl">
    <a href="news.html?url=${url}">
        <div class="pade">
            <div class="list-data">
                <div class="data-image"><img src="../assets/image/${gambar}" alt=""></div>
                <div class="data-desc">
                    <h5 class="capital oswald" style="font-size: 18px; color: #666;">${nama.substr(0,150)}</h5></div>
            </div>
        </div>
    </a>
</script>

     <script src="../assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.tmpl.min.js"></script>
    <script src="../assets/js/jquery.auto-complete.js"></script>
 <script src="../assets/js/action.js"></script>

    <script src="../assets/js/destinasi.js"></script>
<script src="../assets/js/news-list.js"></script>
<script src="../assets/js/news.js"></script>
<script src="../assets/js/news-get.js"></script>




</div>


 <script src="../assets/js/jquery.js"></script>
<script>
	var getDestinationURL = function getDestinationURL(urlParam) {
        var DestinationURL = decodeURIComponent(window.location.search.substring(1)),
            URLvar = DestinationURL.split('&'),
            destinationName, i;
        for (i = 0; i < URLvar.length; i++) {
            destinationName = URLvar[i].split('=');
            if (destinationName[0] === urlParam) {
                return destinationName[1] === undefined ? true : destinationName[1];
            }
        }
    };

    var target_url = getDestinationURL('post_type');
    //alert(target_url)
   if (target_url != "destination") {
    $("#destination_temp").remove();
   }
   else if (target_url != "news" && target_url != "culinary" && target_url != "culture") {
    $("#news_temp").remove();
   }
</script>







