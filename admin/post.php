<?php
require ('../config/secure_login.php');
//echo $start_page;
if ($start_page == 'incomplete') {
	header("location:login.php");
}
else if ($start_page == 'notverify') {
	header("location:verify_account.php");
}

$get =  $lib->show_user_data($id);
$get_all  = $lib->show_user();
$data = $get->fetch(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Post Page - Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="assets/css/ready.css">
	<link rel="stylesheet" href="assets/css/demo.css">
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">

	<style>
		.modal-open .modal.image {
  display: flex !important;
  align-items: center !important;
  .modal-dialog {
    flex-grow: 1;
  }
}
.card-file
{
	border: 4px dashed #59d05d;
}
.modal.image.show{
	z-index: 999999999999999999999999999999999999;
}
.row.editor_parent div.col-xs-12
{
	
	padding-left: 5px;
	padding-right: 5px;
}.row.editor_parent div.col-xs-12 .form-group
{
	padding: 15px 5px;
}


</style>
</head>

<body user_id="<?php echo $id ?>">
	<div class="wrapper">
		<div class="main-header">
			<div class="logo-header">
				<a href="index.php" class="logo">
					Dashboard
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
				<div class="container-fluid">
					
					
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
					
						<li class="nav-item dropdown hidden-caret">
						
						<li class="nav-item dropdown">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <img src="<?php   echo "../assets/image/profile_images/". $data->profile?>" alt="user-img" width="36" class="img-circle"><span ><?php echo substr($name,0,20); ?></span></span> </a>
							<ul class="dropdown-menu dropdown-user">
								<li>
									<div class="user-box">
										<div class="u-img"><img src="<?php   echo "../assets/image/profile_images/". $data->profile?>" alt="user"></div>
										<div class="u-text">
											<h4><?php echo substr($name,0,12); ?></h4>
											<p class="text-muted"><?php  echo $email  ?></p></div>
										</div>
									</li>
									<div class="dropdown-divider"></div>
									
									<a class="dropdown-item" id="logout" href="#"><i class="fa fa-power-off"></i> Logout</a>
									
								</ul>
								<!-- /.dropdown-user -->
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="sidebar">
				<div class="scrollbar-inner sidebar-wrapper">
					<div class="user">
						<div class="photo">
							<img src="<?php   echo "../assets/image/profile_images/". $data->profile?>">
						</div>
						<div class="info">
							<a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?php echo substr($name,0,20); ?>
									<span class="user-level">Administrator</span>
								
								</span>
							</a>
							<div class="clearfix"></div>

							
						</div>
					</div>
					<ul class="nav">
						<li class="nav-item">
							<a href="index.php">
								<i class="la la-dashboard"></i>
								<p>Dashboard</p>
								<span class="badge badge-count"></span>
							</a>
						</li>
							<li class="nav-item active">
							<a href="post.php">
								<i class="la la-table"></i>
								<p>Post</p>
								<span class="badge badge-count"></span>
							</a>
						</li>

					</ul>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="container-fluid">
						<h4 class="page-title">All Post</h4>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									
									<div class="card-header">
										<div class="row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
												<label for="post-title">Post Name</label>
												<input type="" class="form-control" id="post-title" placeholder="Enter post title">
												<small id="name_err" class="form-text text-muted">enter the title for your post</small>
												</div>
											</div>

										
												




											<div class="col-md-4 col-xs-12">
													<div class="form-group">
															<label for="type-select">Post Type</label>
																<select class="form-control input-square" id="type-select">
																		<option selected disabled>Select type-select</option>

																		<option value="destination">Destination</option>
																		<option value="news">News</option>
																		<option value="culinary">Culinary</option>
																		<option value="culture">Culture</option>
																		<option value="transportation">Transportation</option>
																		<option value="homestay">Homestay</option>
																	</select>
															<small id="type_err" class="form-text text-muted">select your post type</small>
													</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
												<label for="post-title">Location</label>
												<input type="" class="form-control" id="location" placeholder="Enter location">
												<small id="location_err" class="form-text text-muted">enter location</small>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6 col-xs-12 disabled">
												<div class="row">
													<div class="col-md-6 col-xs-12">
																<div class="form-group">
														<label for="post-title">Price</label>
														<input type="" class="form-control price_ "  disabled id="price" placeholder="Rp">
														<small id="price_err" class="form-text text-muted">Price</small>
														</div>
													</div>
													<div class="col-md-6 col-xs-12">
																<div class="form-group">
														<label for="post-title">Discount</label>
														<input type="" class="form-control price_"  disabled id="discount" placeholder="Rp">
														<small id="discount_err" class="form-text text-muted">Discount</small>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-xs-12">
												<div class="form-group" style="padding-top: 43px;">
												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#edit-file">Add  Background</button>
												<small id="file_err" class="form-text text-muted text-file">click to insert image file </small>
											</div>
											</div>

											<div class="col-md-2 col-xs-12">
												<div class="form-group" style="padding-top: 43px;">
												<button type="button" class="btn btn-success" id="edit_text">Open Text editor</button>
												<small id="content_err" class="form-text text-muted text-err"></small>
											</div>
											</div>
											<div class="col-md-2 col-xs-12">
												<div class="form-group" style="padding-top: 43px;">
												<button type="button" class="btn btn-success btn-save-post"> Save this Post</button>
												<small id="many_err" class="form-text text-muted text-err"></small>

											</div>
											</div>	
										</div>
										
									</div>

									<div class="card-body" style="display: none;" id="text-card-body" >
										
									</div>
									<div class="card-body">
										<table class="table table-head-bg-success table-striped table-hover">
											<thead>
												<tr>
													<th scope="">No</th>
													<th scope="col">Name</th>
													<th scope="col">Date</th>
													<th scope="col">Type</th>
														<th scope="col">Location</th>
													<th scope="col">Price/Discount</th>
													<th scope="col">Action</th>
												</tr>
											</thead>
											<tbody id="post_component">
												<?php

												$show = $lib->post_list();
												$count = 1;
												while ($data = $show->fetch(PDO::FETCH_OBJ)) {
													$counting = $count++;
													print_r("<tr post_id='".$data->id."''>");
													print_r("<td class='number'>".$counting."</td>");
													print_r("<td>".$data->post_name."</td>");
													print_r("<td>".$data->date_modified."</td>");

													print_r("<td>".$data->post_type."</td>");

													print_r("<td>".$data->location."</td>");
													print_r("<td>"."Rp.".$data->price."/"."Rp.".$data->discount."</td>");	
													if ($data->post_type == "news" || $data->post_type == "culinary" || $data->post_type == "culture"   ) {
														$target_show = "../news.php?url=";
													}
													elseif ($data->post_type == "homestay") {
														$target_show = "../homestay.php?id=";
													}
													elseif ($data->post_type == "transportation") {
														$target_show = "../transportation.php?id=";
													}
													elseif ($data->post_type == "destination") {
														$target_show = "../destination.php?lokasi=";
													}
													


													?>


												
												<td>
													<a href="<?php   echo $target_show.$data->id; ?>" target='_blank'  data-toggle='tooltip' class='btn btn-link btn-info ' data-original-title='Priview'>
													<i class='la la-eye'></i> </a> <button type='button' data-toggle='tooltip' class='btn btn-link btn-warning edit' data-original-title='Edit'> <i class='la la-edit'></i> </button> <button type='button' data-toggle='tooltip' class='btn btn-link btn-danger delete_post' data-original-title='Delete'> <i class='la la-times'></i> </button> </td> </tr></td>

												<?php
												print_r("</tr>");
												}

												?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>



	<!-- loading -->
	<div class="loading">
		<div class="loader">
			<div class="loading-area">
			<span class="loader-def loader-def1"></span>
			<span class="loader-def loader-def2"></span>
			<span class="loader-def loader-def3"></span>
			<span class="loader-def loader-def4"></span>
			<span class="loader-def loader-def5"></span>
		</div>
		</div>
	</div>
	</div>
	<!--loading -->
	<!-- Modal -->
	<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePro" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<h6 class="modal-title"><i class="la la-frown-o"></i> Under Development</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">									
					<p>Currently the pro version of the <b>Ready Dashboard</b> Bootstrap is in progress development</p>
					<p>
					<b>We'll let you know when it's done</b></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal image fade" id="edit-file">
  <div class="modal-dialog modal-lg modal-file" >
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit file</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <label class="col-12 card-file" style="min-width: 500px; min-height: 300px; position: relative; overflow: hidden;" >
       	<input type="file" name="file-editor" accept="image/*"  class="file-editor" style="background: red; height: 100%; width: 100%; position: absolute; left: 0; opacity: 0; z-index: 99;">
       	<img src="" alt="" class="file-show" style="width: 100%; height: 100% ;position: absolute; left: 0; top:0; z-index: 0; filter: brightness(50%); opacity: .3; object-fit:cover;">
       	<div class="button-inner " style="display: flex; height: 100%; width: 100%; align-items: center; position: absolute; top: 0; left: 0; vertical-align: midle; z-index: 9" >
       			<h4 class="text-center" style="margin:0 auto; color:#B2B2B2 ">Drag to Upload</h4>
       		 
       		
       	</div>
       	
       </label>
       <div class="row"><button type="button" class="btn-info btn btn-to-file" style="margin:0 auto;">Click to open file</button></div>
      
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
       
      </div>

    </div>
  </div>
</div>

<!--quick priview -->

 <div class="modal fade" id="quick-priview"   >
 	 <div class="modal-dialog modal-lg modal-priview" >

    <div class="modal-content">
    	<div class="priview-close" data-dismiss="modal">
 		 <button type="button" class="close priview" >&times;</button>
 		 
 	</div>
 	<div class="warning-mode">
 		<h5 class="text-center">Press esc to close</h5>
 	</div>


      
<div class="priview-data " id="priview_content">     

   	
    </div>
    </div>
  </div>
</div>
  </div>


  <!--modal quick edit -->

  <div class="modal fade" id="quick-edit"   >
 	 <div class="modal-dialog modal-lg modal-priview" >

    <div class="modal-content">
    	<div class="priview-close" data-dismiss="modal">
 		 <button type="button" class="close priview" >&times;</button>
 		 
 	</div>
 	<div class="warning-mode">
 		<h5 class="text-center">Press esc to close</h5>
 	</div>
  

	<div class="priview-data " id="edit_content">   
	<div class="container-fluid">  
		<div class="row">
			<div class="card" style="width: 100%;">
			<div class="card-header" id="mode_content">
										<div class="row editor_parent">
											<div class="col-md-2 col-xs-12">
												<div class="form-group">
												<label for="post-title">Post Name</label>
												<input type="" class="form-control" id="edit-post-title" placeholder="Enter post title">
												<small id="name_err" class="form-text text-muted text_name">enter the title for your post</small>
												</div>
											</div>

										
												



											<div class="col-md-2 col-xs-12">
													<div class="form-group">
															<label for="type-select">Post Type</label>
																<select class="form-control input-square" id="type-select-edit">
																		<option selected disabled>Select the option</option>

																		<option value="destination">Destination</option>
																		<option value="news">News</option>
																		<option value="culinary">Culinary</option>
																		<option value="culture">Culture</option>
																		<option value="transportation">Transportation</option>
																		<option value="homestay">Homestay</option>
																	</select>
															<small id="type_err" class="form-text text-muted text_select">select your post type</small>
													</div>
											</div>
											<div class="col-md-2 col-xs-12">
												<div class="form-group">
												<label for="post-title">Location</label>
												<input type="" class="form-control" id="edit_location" placeholder="Location">
												<small id="edit_location_err" class="form-text text-muted text_name">enter the location</small>
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="row">
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
												<label for="post-title">Price</label>
												<input type="" class="form-control price_edit" id="edit_price" placeholder="Rp.">
												<small id="edit_price_err" class="form-text text-muted text_name">Price</small>
												</div>

													</div>
													<div class="col-md-6 col-xs-12">
														<div class="form-group">
												<label for="post-title">Discount</label>
												<input type="" class="form-control price_edit" id="edit-discount" placeholder="Rp.">
												<small id="edit_discount_err" class="form-text text-muted text_name">Discount</small>
												</div>

													</div>
												</div>
											</div>


											<div class="col-md-3 col-xs-12">
												<div class="row">
												<div class="col-md-6 col-xs-11">
													
													<div class="form-group" style="padding-top: 43px;">
													<button type="button" class="btn btn-success" data-toggle="modal" data-target="#edit-file">edit background</button>
													<small id="edit_file_err" class="form-text text-muted text-file">click to insert image file </small>
													</div>
													</div>
											
											<div class="col-md-6 col-xs-12">
												<div class="form-group" style="padding-top: 43px;">
												<button type="button" class="btn btn-success" id="save-edit">save edit</button>
												<small id="many_err" class="form-text text-muted text-err"></small>

											</div>
										</div>
											</div>	
										</div>
									</div>
										
									</div>

									<div class="card-body">
										<textarea name="" class="text_editor" id="tiny_text_edit"></textarea>		
										<input name="image" type="file" accept="image/*" multiple id="upload" class="hidden" onchange="" style="display: none !important;">
									</div>

			</div>
			</div>
			<!----->
		</div>
    </div>
    </div>
  </div>
</div>
  </div>

  <!--end quick edit-->
</body>
<script src="assets/js/core/jquery.3.2.1.min.js"></script>
<script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="assets/js/core/popper.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/plugin/chartist/chartist.min.js"></script>
<script src="assets/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js"></script>
<script src="assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
<script src="assets/js/plugin/jquery-mapael/jquery.mapael.min.js"></script>
<script src="assets/js/plugin/jquery-mapael/maps/world_countries.min.js"></script>
<script src="assets/js/plugin/chart-circle/circles.min.js"></script>
<script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="assets/js/ready.min.js"></script>
<script src="../assets/dist/tinymce/tinymce.min.js"></script>
<script src="assets/js/tiny_setup.js"></script>
<script src="assets/js/admin_post.js"></script>
<script src="../assets/js/handlebars-v4.0.11.js"></script>
<script src="../ajax/ajax_new_post.js"></script>
<script src="../ajax/ajax_delete_post.js"></script>
<script src="../ajax/ajax_edit_post.js"></script>
<script src="../ajax/ajax_update_post.js"></script>
<script src="../ajax/logout.js"></script>
<script src="../ajax/ajax_send_post.js"></script>





<script>
	
jQuery(document).ready(function($) {
    tinyMCE.activeEditor.setContent('');
});
</script>


</html>

