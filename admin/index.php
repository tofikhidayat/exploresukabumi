<?php
require ('../config/secure_login.php');
//echo $start_page;
if ($start_page == 'incomplete') {
	header("location:login.php");
}
else if ($start_page == 'notverify') {
	header("location:verify_account.php");
}

$get =  $lib->show_user_data($id);
$get_all  = $lib->show_user();
$all_user  = $lib->show_user();
$alluser  = $lib->show_user();
$data = $get->fetch(PDO::FETCH_OBJ);
$all_post = $lib->post_list();
$get_msg =  $lib ->show_message();
$num_msg =   $lib ->show_message();
$number_msg = 0; 
$all_admin = 0;
$posts = 0;
$all_days = 0;
$weeks = 0;
$years = 0;
$months = 0;
$days = 0;

while ( $num_msg ->fetch(PDO::FETCH_OBJ)) {
	$number_msg++;
}
while ($demo = $alluser->fetch(PDO::FETCH_OBJ)) {
	$all_admin++;
}

$like_count = 0;
$rating_like = 0;
while ($get_like = $all_post->fetch(PDO::FETCH_OBJ)) {
	$posts++;

$rating_like = $get_like ->like_rating + $rating_like	;

}


						$day = date("d");
						$week = date("w");
						$month= date("m");
						$year = date("Y");


						$show_all  = $lib->show_all_days();
						$show_day  = $lib->show_days($day,$month,$year);
						$show_week  = $lib->show_week($week,$month,$year);
						$show_month  = $lib->show_month($month,$year);
						$show_year  = $lib->show_year($year);
						
						while ($show_all->fetch(PDO::FETCH_OBJ)) {
							$all_days++;
							
						}
						while ($show_day->fetch(PDO::FETCH_OBJ)) {
							$days++;
							
						}
						while ($show_week->fetch(PDO::FETCH_OBJ)) {
							$weeks++;
							
						}
						while ($show_month->fetch(PDO::FETCH_OBJ)) {
							$months++;
							
						}
						while ($show_year->fetch(PDO::FETCH_OBJ)) {
							$years++;
							
						}
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="assets/css/ready.css">
	<link rel="stylesheet" href="assets/css/demo.css">
</head>
<body>

	<div class="wrapper">
		<div class="main-header">
			<div class="logo-header">
				<a href="index.php" class="logo">
					Admin Dashboard 
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
				<div class="container-fluid">
					
					
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center" >
						
											<li class="nav-item dropdown">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <img src="<?php   echo "../assets/image/profile_images/". $data->profile?>" alt="user-img" width="36" class="img-circle"><span ><?php echo substr($name,0,20); ?></span></span> </a>
							<ul class="dropdown-menu dropdown-user">
								<li>
									<div class="user-box">
										<div class="u-img"><img src="<?php   echo "../assets/image/profile_images/". $data->profile?>" alt="user"></div>
										<div class="u-text">
											<h4><?php echo substr($name,0,12); ?></h4>
											<p class="text-muted"><?php  echo $email  ?></p></div>
										</div>
									</li>
									
									<a class="dropdown-item" id="logout" href="#"><i class="fa fa-power-off"></i> Logout</a>
									
								</ul>
								<!-- /.dropdown-user -->
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="sidebar">

				<div class="scrollbar-inner sidebar-wrapper">
					<div class="user">
						<div class="photo">
							<img src="<?php   echo "../assets/image/profile_images/". $data->profile?>">
						</div>
						<div class="info">
							<a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?php echo substr($name,0,20); ?>
									<span class="user-level">Administrator</span>
									
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample" aria-expanded="true" style="">
							
							</div>
						</div>
					</div>
					
					<ul class="nav">

						
						<li class="nav-item active">
							<a href="index.php">
								<i class="la la-dashboard"></i>
								<p>Dashboard</p>
								<span class="badge badge-count"></span>
							</a>
						</li>
						<li class="nav-item">
							<a href="post.php">
								<i class="la la-table"></i>
								<p>Post</p>
								<span class="badge badge-count"></span>
							</a>
						</li>
						
					</ul>
				</div>
			</div>
			<div class="main-panel">
				<div class="content">
					<div class="container-fluid">
						<h4 class="page-title">Dashboard</h4>
						<div class="row">
							<div class="col-md-3">
								<div class="card card-stats text-warning">
									<div class="card-body ">
										<div class="row">
											<div class="col-5">
												<div class="icon-big text-center">
													<i class="la la-users"></i>
												</div>
											</div>
											<div class="col-7 d-flex align-items-center">
												<div class="numbers">
													<p class="card-category">Visitors</p>
													<h4 class="card-title"><?php print($all_days);?></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-stats text-success">
									<div class="card-body ">
										<div class="row">
											<div class="col-5">
												<div class="icon-big text-center">
													<i class="la la-user"></i>
												</div>
											</div>
											<div class="col-7 d-flex align-items-center">
												<div class="numbers">
													<p class="card-category">Admin</p>
													<h4 class="card-title"><?php   print($all_admin);   ?></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-stats text-danger">
									<div class="card-body">
										<div class="row">
											<div class="col-5">
												<div class="icon-big text-center">
													<i class="la la-newspaper-o"></i>
												</div>
											</div>
											<div class="col-7 d-flex align-items-center">
												<div class="numbers">
													<p class="card-category">All Post</p>
													<h4 class="card-title"><?php  print($posts); ?></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-stats text-primary">
									<div class="card-body ">
										<div class="row">
											<div class="col-5">
												<div class="icon-big text-center">
													<i class="la la la-vimeo-square la-vine la-vk la-volume-down la-volume-off la-volume-up la-warning la-wechat"></i>
												</div>
											</div>
											<div class="col-7 d-flex align-items-center">
												<div class="numbers">
													<p class="card-category">Comments</p>
													<h4 class="card-title " ><?php   print($number_msg);?></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						
						<div class="row row-card-no-pd">

					

							<div class="col-md-4">
								<div class="card">
									<div class="card-body">
										<p class="fw-bold mt-1">Total Visitors</p>
										<h4><b><?php  print($all_days) ?></b></h4>
										
									</div>
									<div class="card-footer">
										<ul class="nav">
											<li class="nav-item"><a class="btn btn-default btn-link" href="#"><i class="la la-history"></i> History</a></li>
											<li class="nav-item ml-auto"><a class="btn btn-default btn-link" onclick="window.location = document.URL; return false;" href=""><i class="la la-refresh"></i> Refresh</a></li>
										</ul>
									</div>
									<div class="card-footer">
										<div class="nav pt-2">
											<li class="nav-item pt-2 col-md-7">
												<p class="fw-bold">Maintenance Mode</p>
											</li>
											<li class="nav-item col-md-5">

											<?php   
											$status = "";
											$get_maintenance = $lib->show_maintenance('1');
											$fetch = $get_maintenance->fetch(PDO::FETCH_OBJ);
											//print($fetch->status);
											$status =$fetch->status; 
											if ($status == "true" ) {
												$maint = "btn-danger on";
											}
											else
											{
												$maint = "btn-default off"; 
											}
											 ?>	

												<div class="pull-right">
												<div class="toggle btn btn-round <?php  print($maint); ?> btn-toggle-data " maintenance="false" data-toggle="toggle" style="width: 70px; height: 30px;"><input type="checkbox" checked="" data-toggle="toggle" data-onstyle="danger" data-style="btn-round"><div class="toggle-group"><label class="btn btn-danger toggle-on">On</label><label class="btn btn-default active toggle-off">Off</label><span class="toggle-handle btn btn-default"></span></div></div>
											</div>
											</li>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="card">
									<div class="card-body">
										<div class="progress-card">
											<div class="d-flex justify-content-between mb-1">
												<span class="text-muted">this Year</span>
												<span class="text-muted fw-bold"> <?php  print($years); ?></span>
											</div>
											<div class="progress mb-2" style="height: 7px;">
												<div class="progress-bar bg-success" role="progressbar" style="width: <?php print(100*$years/10000 ."%"); ?> " aria-valuenow="" aria-valuemin="0" aria-valuemax="100" data-toggle="" data-placement="top" ></div>
											</div>
										</div>
										<div class="progress-card">
											<div class="d-flex justify-content-between mb-1">
												<span class="text-muted">This Month</span>
												<span class="text-muted fw-bold"> <?php  print($months); ?></span>
											</div>
											<div class="progress mb-2" style="height: 7px;">
												<div class="progress-bar bg-info" role="progressbar"  style="width: <?php print(100*$months/1000 ."%"); ?> " aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-toggle="" data-placement="top" 65%"></div>
											</div>
										</div>
										<div class="progress-card">
											<div class="d-flex justify-content-between mb-1">
												<span class="text-muted">This Week</span>
												<span class="text-muted fw-bold"> <?php  print($weeks); ?></span>
											</div>
											<div class="progress mb-2" style="height: 7px;">
												<div class="progress-bar bg-primary" role="progressbar"  style="width: <?php print(100*$weeks/500 ."%"); ?> " aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" data-toggle="" data-placement="top" ></div>
											</div>
										</div>
										<div class="progress-card">
											<div class="d-flex justify-content-between mb-1">
												<span class="text-muted">This Day</span>
												<span class="text-muted fw-bold"><?php  print($days); ?></span>
											</div>
											<div class="progress mb-2" style="height: 7px;">
												<div class="progress-bar bg-warning" role="progressbar"  style="width: <?php print(100*$years/100 ."%"); ?> "aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-toggle="" data-placement="top" ></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="card card-stats">
									<div class="card-body">
										<p class="fw-bold mt-1">Statistic</p>
										<div class="row ">
											<div class="col-12">
												<div class="progress-card">
											<div class="d-flex justify-content-between mb-1">
												<span class="text-muted"></span>
												<span class="text-muted fw-bold"><?php    print($rating_like);?></span>
											</div>
											<div class="progress mb-2" style="height: 7px;">
												<div class="progress-bar bg-danger" role="progressbar" style=" width:<?php    print(100*$rating_like/10000);?>%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" ></div>
											</div>
										</div>
											
										</div>
										<hr/>
										<div class="" style="padding-left: 10px">
											<div class="row">
											<div class="col-5">
												<div class="icon-big text-center">
													<i class="la la-heart-o text-danger"></i>
												</div>
											</div>
											<div class="col-7 d-flex align-items-center">
												<div class="numbers">
													<p class="card-category">liked</p>
													<h4 class="card-title">+<?php    print($rating_like);?></h4>
												</div>
											</div>
										</div></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class="col-md-6">
								<div class="card">
									<div class="card-header ">
										<h4 class="card-title">All Admin</h4>
									</div>
									<div class="card-body">
										<table class="table table-head-bg-success table-striped table-hover">
											<thead>
												<tr>
													<th scope="col">No</th>
													<th scope="col">Name</th>
													<th scope="col">Email</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>

												<?php
												$number_list = 0;
												while ($show  = $get_all -> fetch(PDO::FETCH_OBJ)) {
													$number_list++;
													
													?>
												<tr>
													<td><?php 	echo $number_list; ?></td>
													<td><?php   echo  $show -> name;  ?></td>
													<td><?php   echo  $show -> email; ?></td>
													<td>online</td>
												</tr>

												
												<?php
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-header ">
										<h4 class="card-title">Comments</h4>
									</div>
									<div class="card-body">
										<table class="table table-head-bg-success table-striped table-hover">
											<thead>
												<tr>
													<th scope="col">No</th>
													<th scope="col">Date</th>
													<th scope="col">From</th>
													<th scope="col">Content</th>
													<th scope="col">action</th>
												</tr>
											</thead>
											<tbody>
												
<?php 
												$num = 0; 
												while ($msg = $get_msg->fetch(PDO::FETCH_OBJ) ) {
				
												$num++;
											


												  ?>
												<tr class="list_msg">
													<td><?php   print($num);  ?></td>
													<td><?php   print($msg->date);  ?></td>
													<td><?php   print($msg->from_user);  ?></td>
													<td  msg="<?php   print($msg->content);  ?>" ><?php   print(substr($msg->content,0,10));  ?></td>
													<td> 
														<button type='button' data-toggle='tooltip' class='btn btn-link btn-warning btn-xs look pull-left' data-original-title='show'> <i class='la la-eye'></i> </button>
													 <button type='button' data-toggle='tooltip' class='btn btn-link btn-danger btn-xs delete_mesage pull-left' data-original-title='Delete' msg_id="<?php   print($msg->increment);  ?>"> <i class='la la-times'></i> </button>

													</td>

												</tr>

												<?php 		} ?>


												<tr>	
													<td colspan="5"><button type="" class="btn btn-danger pull-right btn-sm delete_all_message">Remove All</button></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
				</div>
				
			</div>
		</div>
	</div>

	<!-- Modal -->

	<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePro" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<h6 class="modal-title"><i class="la la-frown-o"></i> Under Development</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">									
					<p>Currently the pro version of the <b>Ready Dashboard</b> Bootstrap is in progress development</p>
					<p>
						<b>We'll let you know when it's done</b></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
		
</body>
<script src="assets/js/core/jquery.3.2.1.min.js"></script>
<script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="assets/js/core/popper.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/plugin/chartist/chartist.min.js"></script>
<script src="assets/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js"></script>
<script src="assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
<script src="assets/js/plugin/jquery-mapael/jquery.mapael.min.js"></script>
<script src="assets/js/plugin/jquery-mapael/maps/world_countries.min.js"></script>
<script src="assets/js/plugin/chart-circle/circles.min.js"></script>
<script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="assets/js/ready.min.js"></script>
<script src="assets/js/demo.js"></script>
<script src="../ajax/logout.js"></script>
<script src="../ajax/ajax_delete_message.js"></script>
<script src="../ajax/maintenance.js"></script>
</html>
