<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="generator" content="EXploresukabumi" >
    <meta name="description" content="temukan potongan surga yang hilang  sukabumi di sukabumi ,sukabumi terkenal akan kekayaan alam dan budayanya yang mempesona setiap pasang mata ">
    <meta name="keywords" content="exploresukabumi,sukabumi,explore,pariwisata,budaya,musik,kuliner,alam,geopark,pantai,gunung,pegunungan,bebas,alam">
    <meta name="author" content="syntac">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="tQ7xEnvjr7UdaYnzNYfRL0tX3wPa6TbOwkx__s2JZgk" />
    <meta property="og:url" content="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; ?>" class="meta-url">
    <meta property="og:title" content=""   class="meta-title">
      <title></title>
      <link rel="icon" type="img/ico" href="assets/image/favicon.ico">
      <link rel="stylesheet" type="text/css" href="../assets/css/main.style.css">
      <link href="../https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
      <link href="../https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
      <link href="../assets/css/owl.carousel.css" rel="stylesheet">
      <link href="../assets/css/destination.css" rel="stylesheet">
       <link href="../assets/css/singup.css" rel="stylesheet">
      <link href="../assets/css/destination-data.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
      <link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

      <script src="../assets/js/jquery.js"></script>

   </head>

   <section class="singup_container">
     <div class="singup-bg">
     	
     	<div class="singup_body">
     		<form action="#" method="post" accept-charset="utf-8">
     		
     		<div class="head-singup">
     				<label class="head-data"><input type="file" name="" value="" class="profile_selector" accept="image/*">
     					<img src="../assets/image/camera.png" alt="" class="camera">
     					<img src="" alt="" class="file profile">
     				</label>
     		</div>
     	<div class="singup-content">
     		<div class="full-width content-form">
     			<div class="singup-label">
     				<label for="email">Email</label>
     			</div>
     			<div class="singup-form">
     				<input type="" name="" id="email" class="text getval" value="">
     			</div>
     		</div>
     		<div class="full-width content-form">
     			<div class="singup-label">
     				<label for="username">Username</label>
     			</div>
     			<div class="singup-form">
     				<input type="text" name="" id="username"  class="text getval" value="">
     			</div>
     		</div>
     		<div class="full-width content-form">
     			<div class="singup-label">
     				<label for="password">Password</label>
     			</div>
     			<div class="singup-form">
     				<input type="password" name="password" id="password" class="password-secure" value="">
     				<button class="show-hide-password fa fa-eye pass" show="false" type="button"></button>
     			</div>
     		</div>
     		<div class="full-width content-form">
     			<div class="singup-label">
     				<label for="repassword">Retype Password</label>
     			</div>
     			<div class="singup-form">
     				<input type="password" name="repassword" class="getval password-secure"  id="repassword"  value="">
     				<button class="show-hide-password fa fa-eye repass" show="false" type="button"></button>
     			</div>
     		</div>

     	</div>
     	<div class="singup-foot">
     		<div class="foot-content allready">
     			<a href="login.php" title="login">Allready have an account ?</a>
     		</div>
     		<div class="foot-content submit-area">
     			<button class="submit" type="button">Submit</button>
     		</div>
     	</div>
     	</form>
     </div>

     	
     </div>
     <div class="loading">
    <div class="loader">
      <div class="loading-area">
      <span class="loader-def loader-def1"></span>
      <span class="loader-def loader-def2"></span>
      <span class="loader-def loader-def3"></span>
      <span class="loader-def loader-def4"></span>
      <span class="loader-def loader-def5"></span>
    </div>
    </div>
  </div>
   </section>
   </body>
   <script >
  function loading() {
    $(".loading").fadeIn('fast');
}

function endload() {
    $(".loading").fadeOut('fast');
}
</script> 
   <script src="../assets/js/swal.js"></script>
   <script src="../assets/js/jquery.passwordstrength.js"></script>
   <script src="../assets/js/singup_.js" ></script>
</html>

<script>
	   
</script>



	