jQuery(document).ready(function($) {
tinymce.init({
  selector: 'textarea.text_editor',
  height: 550,
  theme: 'modern',
    paste_data_images: true,
   setup: function (ed) {	
        ed.on('init', function (e) {
            ed.execCommand("fontName", false, "Arial");
             ed.execCommand("fontSize", false, "16pt");
        }),
        ed.addButton('Save_BUtton', {
      text: 'Save Document',
      icon: false,
      onclick: function () {
       save();
      }
    }), 
        ed.addButton('cancel_BUtton', {
      text: 'Cancel and Exit',
      icon: false,
      onclick: function () {
cancel();
            


      }
      });
    },


  fontsize_formats: "4pt 6pt 8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 28pt 36pt 42pt 60pt 72pt",
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  emoticons paste  linkchecker contextmenu colorpicker textpattern  stickytoolbar',

   toolbar2 : 'formatselect | codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify  | bullist numlist outdent indent | link image | insertfile undo redo | forecolor backcolor emoticons | code |outdent indent  | removeformat ',
  toolbar1: "print  preview table View   media ",
    
  image_advtab: true,
  font_formats: 'Arial=arial,helvetica,sans-serif;Open Sans=Open Sans, sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n;Rubik= Rubik, sans-serif;Lato=Lato, sans-serif;Ibm Plex Sans = IBM Plex Sans, sans-serif;Monserat=Montserrat, sans-serif;Raleway=Raleway,sans-serif;Ubuntu=Ubuntu, sans-serif;Play Fair =Playfair Display, serif;Roboto = Roboto, sans-serif;Odor Mean Chey=	Odor Mean Chey, cursive',
  paste_data_images: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
        extended_valid_elements : "em[class|name|id]",
        valid_children : "+body[style], +style[type]",
        apply_source_formatting : false,                //added option
        verify_html : false,                            //added option
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css',
    '//fonts.googleapis.com/css?family=IBM+Plex+Sans|Lato|Montserrat|Odor+Mean+Chey|Open+Sans|Playfair+Display|Raleway|Roboto|Rubik|Ubuntu'
  ],
   image_advtab: true,
    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            callback(e.target.result, {
              alt: ''
            });
          };
          reader.readAsDataURL(file);
        });
      }
    },
 });

});


tinymce.PluginManager.add('stickytoolbar', function(editor, url) {
  editor.on('init', function() {
   // setSticky();
  });
  
  $(window).on('scroll', function() {
    //setSticky();
  });
  
  function setSticky() {
    var container = editor.editorContainer;
    var toolbars = $(container).find('.mce-toolbar-grp');
    var statusbar = $(container).find('.mce-statusbar');
    
    if (isSticky()) {
      $(container).css({
        paddingTop: toolbars.outerHeight()
      });
      
      if (isAtBottom()) {
        toolbars.css({
          top: 'auto',
          bottom: statusbar.outerHeight(),
          position: 'absolute',
          width: '100%',
          borderBottom: 'none'
        }); 
      } else {
        toolbars.css({
          top: 0,
          bottom: 'auto',
          position: 'fixed',
          width: $(container).width(),
          borderBottom: '1px solid rgba(0,0,0,0.2)'
        });       
      }
    } else {
      $(container).css({
        paddingTop: 0
      });
      
      toolbars.css({
        position: 'relative',
        width: 'auto',
        borderBottom: 'none'
      });
    }
  }
  
  function isSticky() {
    var container = editor.editorContainer,
      editorTop = container.getBoundingClientRect().top;
    
    if (editorTop < 0) {
      return true;
    }
    
    return false;
  }
  
  function isAtBottom() {
    var container = editor.editorContainer,
      editorTop = container.getBoundingClientRect().top;
    
    var toolbarHeight = $(container).find('.mce-toolbar-grp').outerHeight();
    var footerHeight = $(container).find('.mce-statusbar').outerHeight();
    
    var hiddenHeight = -($(container).outerHeight() - toolbarHeight - footerHeight);
    
    if (editorTop < hiddenHeight) {
      return true;
    }
    
    return false;
  }
});
