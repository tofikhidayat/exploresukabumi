function show_editor(mode) {
    $("#quick-edit").modal('show');
    if (mode == "text_only") {
        $("#mode_content").hide();

    } else {
        $("#mode_content").show();
    }
}

function loading() {
    $(".loading").fadeIn('fast');
}

function endload() {
    $(".loading").fadeOut('fast');
}

//sett popup
$(".btn-to-file").click(function() {
    $(".file-editor").trigger('click');

});




$(".close-err").click(function() {
    $(".file-editor").val('');
    $(".file-show").attr('src', '');
    $(".text-file").text("click to insert image file");

});
jQuery(document).ready(function($) {
    $(".card-file").on('dragover', function(event) {
        $(this).css('border-color', '#ff646d ');
    });
    $(".card-file").on('dragend dragleave', function(event) {

        $(this).css('border-color', '#59d05d ');
    })


});
$(function() {
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'focus'
    })
})



$("#edit_text").click(function(event) {

    show_editor('text_only')

    var post_content = tinymce.get("tiny_text_edit").getContent().replace(/(\r\n|\n|\r)/gm, "");
});


$(".close").click(function(event) {
    $(this).closest('.popup').fadeOut('fast');
});



$(".logout").click(function(event) {
    var log_conf = confirm('are you want to logout ??');
    if (log_conf == true) {
        $.ajax({
            url: '../config/logout.php',
            success: function() {
                console.log('result');
                window.location = "login.php";
            }
        });
    }
});

function save() {
    c = confirm('are you want to save this document ?')
    if (c == true) {


    }
}

function cancel() {
    c = confirm('if you cancel thi document has can permanet remove ?');
    if (c = true) {
        window.location = 'list_post.php';
    }
}



//type content


$("#post-title, #location , #edit_location , #edit-post-title").keyup(function(event) {
    if ($(this).val().length > 4) {
        $(this).closest('.form-group').find('small').removeClass('text-danger').addClass('text-success').text('complete');
    } 
    else {
        $(this).closest('.form-group').find('small').removeClass('text-success').addClass('text-danger').text('enter complete this field');
    }
});


$("#price , #discount , #edit-discount , #edit_price").keyup(function(event) {
    if ($(this).val().length > 0) {
        if ($(this).val() / $(this).val() != 1 && $(this).val() / $(this).val() != 0 ) {
            $(this).closest('.form-group').find('small').removeClass('text-success').addClass('text-danger').text('please inser numeric only');
        }
        else
        {
          $(this).closest('.form-group').find('small').removeClass('text-danger').addClass('text-success').text('complete');   
        }
       
    } else {
        $(this).closest('.form-group').find('small').removeClass('text-success').addClass('text-danger').text('enter the price ');
    }
});



$("#type-select").change(function(event) {

    if ($(this).val() == "transportation" || $(this).val() == "homestay") {
        $(this).closest('.form-group').find('small').removeClass('text-danger').addClass('text-success').text('complete');
        $(".price_").removeAttr('disabled');
    }
    else if($(this).val() != "")
    {
         $(this).closest('.form-group').find('small').removeClass('text-danger').addClass('text-success').text('complete');
          $(".price_").attr('disabled',true);
    }

    else {
        $(this).closest('.form-group').find('small').removeClass('text-success').addClass('text-danger').text('select your post type');
        $(".price_").attr('disabled',true);
    }

});

$("#type-select-edit").change(function(event) {
   if ($(this).val() == "transportation" || $(this).val() == "homestay") {
       $(".price_edit").removeAttr('disabled');
   }
   else
   {
    $(".price_edit").attr('disabled', true);
   }
});


jQuery(document).ready(function($) {
    tinyMCE.activeEditor.setContent('');
});