$(document).on('click', '.delete_post', function(event) {
		var id =  $(this).closest('tr').attr('post_id');
		function clear()
		{
		$("tr[post_id="+id+"]").slideUp('fast');
		setTimeout(function(){
		$("tr[post_id="+id+"]").remove();
		for (var i = 1; i <= $("#post_component tr").length ; i++) {
		        $("#post_component tr:nth-child(" +i+") .number").text(i);
		    	}
		},200)
		}
		$.ajax({
			url: '../config/delete_post.php',
			type: 'POST',
			data: {id:id },
			success:function(response){
				clear();
				console.log(response);


			}
		})
		
});