//save edit 
function update_success(id,name,date,type,location,price)
{   
    var goto;
    if (type == "destination") {
        goto = "../destination.php?lokasi="+id;
    }
    else if(type == "news" || type == "culture" || type == "culinary")
    {
        goto = "news.php?url="+id;
    }


    var number = $("#post_component tr").length ;
    var temp1 = "<td class='number'></td>";
    var temp2 = "<td>"+name+"</td>";
     var temp2a = "<td>"+date+"</td>";
    var temp3 = "<td>"+type+"</td> <td>"+location+"</td> <td>"+price+"</td> ";
    var temp4 = "<td>  <a href='"+goto+"' target='_blank' data-toggle='tooltip' class='btn btn-link btn-info' data-original-title='Priview' ><i class='la la-eye'></i></a>  </button> <button type='button' data-toggle='tooltip' class='btn btn-link btn-warning edit' data-original-title='Edit'> <i class='la la-edit'></i> </button><button type='button' data-toggle='tooltip' class='btn btn-link btn-danger delete_post' data-original-title='Delete'> <i class='la la-times'></i> </button></td>";

    var temp = temp1 + temp2 +temp2a + temp3 + temp4;
    $("tr[post_id="+id+"]").empty().append(temp)
    for (var i = 1; i <= $("#post_component tr").length ; i++) {
        $("#post_component tr:nth-child(" +i+") .number").text(i);
    }
    clear_data();
    get_function_bs();
}


$(document).on('click', '#save-edit', function(event) {
	loading();
    
	var file_editor = $(".file-editor");
	var extfile = $(".file-show").attr('src').split("/").pop();
	var content = tinymce.get("tiny_text_edit").getContent().replace(/(\r\n|\n|\r)/gm, "");
	var post_name = $("#edit-post-title").val();
	var post_type = $("#type-select-edit").val();
	var id = $(this).attr('post_id');
    if (post_type == "homestay" || post_type == "transportation" ) {
    var edit_price =    $("#edit_price").val();
    var edit_discount = $("#edit-discount").val();    
    }
    else{
        var edit_price =  "0";
        var edit_discount = "0";
    }
    var edit_location  = $("#edit_location").val();
    var date = new Date();
	var get_date =  date.getDate() +"/"+  date.getMonth() + "/" + date.getFullYear() + "/" + date.getHours() +"/" + date.getMinutes();
    var file_name = $(".file-show").attr('src').split("/").pop();
    if (file_editor.val() != "") {

		
		send_data_update();

	}
    else
    {
        
        send_data_update();
    }
	function send_data_update(){



		$.ajax({
		url: '../config/update_post.php',
		type: 'POST',
		data: {id: id,post_name:post_name,  post_type:post_type,post_images:file_name,post_content:content,edit_location:edit_location,edit_price:edit_price,edit_discount:edit_discount},
		success:function(response){
			console.log(response);
			if (response == "success") {
               
                    var price ="Rp."+edit_price+"/Rp."+edit_discount;

    				update_success(id,post_name,get_date,post_type,edit_location,price);
			}
			$("#quick-edit").modal('hide');

		}
		})
	}




	endload();
	
});
