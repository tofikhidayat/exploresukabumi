$(document).on('click', '.priview-close', function(event) {
		event.preventDefault();
		$("#priview_content").empty();
		//clear_data();
		
	});
	$(document).on('click', '.quick-priview-data', function(event) {
		event.preventDefault();
		loading();
		var post_type =  $(this).attr('type').toLowerCase();
		var post_id = $(this).attr('id');
			$.ajax({
				url: '../config/priview_temp.php',
				type: 'POST',
				data: {post_type:post_type,post_id:post_id,},
				success:function(result)
				{
					$("#priview_content").append(result);
						setTimeout(function(){
							endload();
					},1000)	
					
				}
			})
			
			
	});
	
	$(document).keyup(function(event) {
		if (event.keyCode === 27){
			$(".priview-close").trigger('click');
		}
	});


	//ImageEditor
$(document).on('click', '.edit', function(event) {
	clear_data();
	var id =  $(this).closest('tr').attr('post_id');
	$.ajax({
		url: '../config/priview_edit.php',
		type: 'POST',
		data: {id: id},
	success:function(response){
		var json_data = $.parseJSON(response); 
		$("#edit-post-title").val(json_data.post_name);
		$("#type-select-edit").val(json_data.post_type);
		$("#edit_price").val(json_data.price);
		if (json_data.post_type == 'transportation' || json_data.post_type == 'homestay') {
			$(".price_edit").attr('disabled', false);
		}
		else
		{
		$(".price_edit").attr('disabled', true);
}
		$("#edit_discount").val(json_data.discount);
		$("#edit_location").val(json_data.location);
		$(".file-show").attr('src', '../assets/image/post_images/'+json_data.post_images);
		$("#save-edit").attr('post_id', json_data.id);
		 tinyMCE.activeEditor.setContent(json_data.post_content);


		//	$("#edit_content").prepend(response);
	}
	})
	  show_editor('full_mode');


});
