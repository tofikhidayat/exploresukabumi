function errcde(target,text){
	$(target).text(text);
	$(target).addClass('text-danger');
}
function get_function_bs()
{
     $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({html:true,trigger:'focus'})
}


function clear_data()
{
    $("#post-title").val("");
    $("#name_err").text('enter the title for your post');
    $("#name_err").removeClass('text_success');
    $("#type-select").val('Select the option');
    $("#type_err").text('select your post type');
    $("#type_err").removeClass('text_success');
    $("#file_err").text('click to insert image file');
    $("#file_err").removeClass('text_success');
    $('input[type=file]').val('');
    $(".file-show").attr('src', '');
    $("#content_err").text('');
    tinyMCE.activeEditor.setContent('');
    $("#many_err").text('');
    $("#location").val('');
    $("#price").val("");
    $("#discount,#edit_location,#edit_price,#edit-discount").val("");


    }   
function send_success(id,name,date,type,location,price)
{   
    var goto;
    if (type == "destination") {
        goto = "../destination.php?lokasi="+id;
    }
    else if(type == "news" || type == "culture" || type == "culinary")
    {
        goto = "news.php?url="+id;
    }
     else if(type == "transportation")
    {
        goto = "transportation.php?url="+id;
    }
     else if(type == "homestay")
    {
        goto = "homestay.php?url="+id;
    }
    var number = $("#post_component tr").length ;
    var temp1 = "<tr  post_id='"+id+"'><td class='number'></td>";
    var temp2 = "<td>"+name+"</td>";
     var temp2a = "<td>"+date+"</td>";
    var temp3 = "<td>"+type+"</td><td>"+location+"</td> <td>"+price+"</td>";
    var temp4 = " <td><a href='"+goto+"' target='_blank' data-toggle='tooltip' class='btn btn-link btn-info ' data-original-title='Priview'> <i class='la la-eye'></i> </a>  <button type='button' data-toggle='tooltip' class='btn btn-link btn-warning ' data-original-title='Edit'> <i class='la la-edit'></i> </button><button type='button' data-toggle='tooltip' class='btn btn-link btn-danger delete_post' data-original-title='Delete'> <i class='la la-times'></i> </button></td></tr></td>";
    var temp = temp1 + temp2 +temp2a + temp3 + temp4;
    $("#post_component").prepend(temp);
    for (var i = 1; i <= $("#post_component tr").length ; i++) {
        $("#post_component tr:nth-child(" +i+") .number").text(i);
    }
    get_function_bs();
}



$(".file-editor").change(function(event) {
    loading();
    if ($(".file-editor").val() == "") {
        $(".text-file").text("click to insert image file").removeClass('text-success').addClass('text-danger');
    } else {
     

        var input = $(".file-editor");

    filedata = new FormData();
    filedata.append('file', input);
    var input = $("input[type=file]")[0].files[0];
    filedata = new FormData();

    filedata.append('file', input);
  

    $.ajax({
        url: '../config/send_file_post.php',
        type: 'POST',
        data: filedata,
        contentType: false,
        processData: false,
        success: function(response) {
            console.log(response);
            if (response == "nofile") {

                errcde("#file_err" , "please upload a image");
            }
            else if (response == "size_err")
            {
                errcde("#file_err" , "upload  image unnder 5mb");
            }
             else if (response == "format_error")
            {
                errcde("#file_err" , "upload  png,jpg,svg or gift only");
            }
             else if (response.substr(-7) == "success")
            {
                file_id = response.replace("success",'');
                  $(".text-file").text("File uploaded").removeClass('text-danger').addClass('text-success');
                  $(".file-show").attr('src', '../assets/image/post_images/'+file_id);
            }
            else{
                alert("failed");
                $(".text-file").text("oop an error").removeClass('text-success').addClass('text-danger');
            } endload()

        }
    });
        
    }
   
});




























$(".btn-save-post").click(function(event) {
    loading();
 var type_selector = $("#type-select");
 if ($(".file-editor").val() == "") {
     $(".text-file").text("click to insert image file").removeClass('text-success').addClass('text-danger');
 }
  else if (type_selector.val() == "transportation" || type_selector.val() == "homestay") {
       if($("#price").val() =="")
       {
          errcde("#price_err" , "please complete this field");
          endload()

       }
       else if($("#discount").val() =="")
       {
          errcde("#discount_err" , "please complete this field");
          endload()
       }
       else
       {
        send_data()
       }
    }

    else
    {
        send_data()
    }
    endload();


 });





function send_data(){
 var date, date_utc, by_id,  post_name, post_type, post_content,file_id,id_post,price,discount,location,file_name;
    date = new Date();
    date_utc = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear() + '/' + date.getHours() + '/' + date.getMinutes();
    by_id = $("body").attr('user_id');
    post_name = $("#post-title").val();
    post_type = $("#type-select").val();
    location = $("#location").val();
    file_name = $(".file-show").attr('src').split("/").pop();
    post_content = tinymce.get("tiny_text_edit").getContent().replace(/(\r\n|\n|\r)/gm, "");
    if (post_type == "transportation" || post_type == "homestay") {
    price = $("#price").val();
    discount = $("#discount").val();
    }
    else
    {
    price = 0;
    discount = 0;
    }
  
    $.ajax({
        url: '../config/save_post.php',
        type: 'POST',
        data: {
            by_id: by_id,
            date_modified: date_utc,
            post_name: post_name,
            post_type: post_type,
            post_content: post_content,
            location:location,
            file_name:file_name,    
            price:price,
            discount:discount,
        },
        success: function(response) {
        	if (response == "name_error") {
        		errcde("#name_err" , "please complete name field");
        	}
        	else if(response == "location_error")
        	{
        		errcde("#location_err" , "please enter location");
        	}
            else if(response == "type_error")
            {
                errcde("#type_err" , "please select post type");
            }

        	else if (response == "no_file") {
        	errcde("#file_err" , "please upload image for background");
        	}

        	else if (response == "content_error") {
        		errcde("#content_err" , "please write description");
        	}
            
        	else if (response.substr(-7) == "success") {
        		console.log("sucessed send");
        		$(this).closest('.form-group').addClass('text-success').removeClass('text-danger').text('success');
                id_post = response.replace('success','')
                var price_total = "Rp."+price+"/Rp."+discount;
                send_success(id_post,post_name,date_utc,post_type,location,price_total);
                clear_data();
                tinyMCE.activeEditor.setContent('');
        	}
           	else {
                console.log('somethingtrong   ' + response)
                $(this).closest('.form-group').find('small').addClass('text-danger').removeClass('text-success').text('oops no internet connection');
            }
            
            endload();

        }
    })
}