<?php
require ("config/get_main.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="generator" content="EXploresukabumi" >
    <meta name="description" content="temukan potongan surga yang hilang  sukabumi di sukabumi ,sukabumi terkenal akan kekayaan alam dan budayanya yang mempesona setiap pasang mata ">
    <meta name="keywords" content="exploresukabumi,sukabumi,explore,pariwisata,budaya,musik,kuliner,alam,geopark,pantai,gunung,pegunungan,bebas,alam">
   <meta name="author" content="syntac">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="tQ7xEnvjr7UdaYnzNYfRL0tX3wPa6TbOwkx__s2JZgk" />
    <meta property="og:url" content="" class="meta-url">
    <meta property="og:title" content=""   class="meta-title">
    <title></title>
    <link rel="icon" type="img/ico" href="assets/image/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/css/main.style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/destination.css" rel="stylesheet">
    <link href="assets/css/galery.css" rel="stylesheet">
    <link href="assets/css/news.css" rel="stylesheet">
    <link href="assets/css/news-new.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
     <script>
       
           
       
    </script>
   
</head>
<header id="header" class="header header-devault">
    <div class="container">
      <div class="logo"><a href="index.php"><img src="assets/image/logo.png" alt=""></a></div>
        <div class="navbar">
            <button class="navbar-toggle devault-button-nav " nav-show="true"><span></span><span></span><span></span></button>
            <nav>
                <ul>
                    <li class="uppercase"><a href="index.php">beranda</a></li>
                    <li class="uppercase"><a href="destinations.php">destinasi</a></li>
                    <li class="uppercase"><a href="gallery.php">galery</a></li>
                    <li class="uppercase active"><a href="news-list.php" class="uppercase">berita</a></li>
                    <li class="uppercase akomodasi pointer "><span  style="color: #383838 !important">akomodasi</span> 
                            <ul class="capital">
                                <li><a href="homestay.php" title="">penginapan</a> </li>
                                <li> <a href="transportation.php" title="">transportasi</a> </li>
                            </ul>
                        </li>
                    <li class="p-0 search-unres-s">
                        <button type="button" class="search-unresponsive" active="false"><i class="fa fa-search "></i></button>
                    </li>
                    <li class="search-res">
                        <div class="ci">
                            <input type="text" name="" placeholder="Cari disini">
                            <button type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<div class="search-unres inactive">
    <div class="search-result ">
        <div class="container">
            <div class="research">
                <div class="search-title"><span>Cari destinasi , dan Artikel lainya</span></div>
                <div class="input-search">
                    <div class="ci">
                        <input type="text" name="" placeholder="Cari disini">
                        <button type="button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="page-content-parent">
        <div class="parent-data">
            <div class="heading">
                <div class="intittle">
                    <h1 id="name"></h1></div>
                <div class="sub-time">
                    <h6><div class="time"><i class="fa fa-clock-o fs-20"></i><span class="fs-14" id="time"></span></div><div class="time"><i class="fa fa-user-circle-o fs-20"></i><span class="fs-14" id="by"></span></div><div class="time"><i class="fa fa-paperclip fs-20"></i><span class="fs-14" id="type"></span></div></h6></div>
                    <div class="rate-area pb-40 hiden" >
                  <div class='rate' style="left: 5px; top: -5px;">
                    <div class="sub-rate" rating=''>
                        <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="page-content-m">
                <div class=" image-page "><img src="" alt="" id="news-image">
                    <div class="credit-tag">
                        <h6>Credits By : <span id="credit"></span> </h6></div>
                </div>
                <div class="page-desc-m">
                    <p id="page-descript" class="fs-16 " style="color: #666;"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="category">
        <div class="search">
            <input type="text" name="" class="search-news" placeholder="Cari Berita">
            <button type="button" class=""> <i class="fa fa-search"></i></button>
        </div>
        <div class="category-title">
            <h3 class="fs-18">Kategori</h3></div>
        <div class="list-category">
            <ul>
                <li><span>Air terjun</span></li>
                <li><span>Pantai</span></li>
                <li><span>liburan</span></li>
                <li><span>Upacara Adat</span></li>
            </ul>
        </div>
        <div class="terkait">
            <div class="category-title">
                <h3 class="fs-18">Berita Terkait</h3></div>
            <div id="list-data"></div>
        </div>
    </div>
</div>
</div>
 <!--share area -->
   <div class="share-area py-10">
      <div class="container">
         <div class="orvfo share-me">
            <button type="button" class="capital share-button-to share-me-please"> <i class="fa fa-share-alt fs-25"></i> <span class="pl-10 fs-15" style="top:-3px">bagikan</span></button>
          
         </div>

         <div class="orvfo rate-me">
            <button type="button" class=" capital like-me" liked="false" ><i class="fs-25"></i><span class="pl-10 fs-15 " style="top:-3px">sukai</span></button>
         </div>
      </div>
   </div>


  <!--    footer area -->
        <footer class="footer">
            <div class="container">
                <div class="foot-logo ">
                    <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.php" title=""><img src="assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
                    <div class="some">
                        <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
                        <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3></div>
                </div>
                <div class="foot-nav">
                    <div class="nav-title mb-20">
                    
                    </div>
                    <div class="nav-body">
                        <nav>
                           <ul>
                               <li>
                                  <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.php" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.php" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.php" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.php" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.php" title="news">transportasi</a> </li>
                        </ul>
                               </li>
                                <li>
                                   <ul>
                                       <li class="capital"><span>hubungi kami</span></li>
                                       <li><p>Telepon</p></li>
                                       <li><p>+123 456 789 </p></li>
                                       <li><p>Email</p></li>
                                       <li><p>eksukabumi@hotmail.com</p></li>
                                      
                                   </ul>
                               </li>

                                <li>
                                   <ul>
                                       <li class="capital"><span>sosial media</span></li>
                                       <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                                        <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                                         <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                                          <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                                   </ul>
                               </li>
                           </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>

   <div class="share-popup in inactive">
     <div class="share-container">
       <div class="share-area">
       <div class="share-title pb-15">
         <h3 class="fs-20 oswald text-left capital">bagikan ke</h3>
       </div>
       <div class="share-content">
         <ul class="list-none">
           <li><button type="" class="share-to to-fb fs-14 capital" to="fb"><i class="fs-18 fa fa-facebook"></i> facebook </button></li>
           <li><button type="" class="share-to to-tw fs-14 capital" to="tw"><i class="fs-18 fa fa-twitter"></i> twitter </button></li>
           <li><button type="" class="share-to to-ig fs-14 capital" to="ig"><i class="fs-18 fa fa-instagram"></i> instagram </button></li>
           <li><button type="" class="share-to to-gp fs-14 capital" to="gp"><i class="fs-18 fa fa-google-plus  "></i> google plus </button></li>
           <li><button type="" class="share-to to-url fs-14 capital" to="url"><i class="fs-18 fa fa-link"></i> salin url </button></li>
         </ul>
       </div> 
       <div class="buttton-close">
         <button type="button" onclick="close_share()"></button>
       </div>  
       </div>
     </div>
     <div class="close-share"></div>
   </div>

       
        
        <div class="search-popup inactive">
            <button type="button" class="fa fa-close-o dismis-c"></button>
              <div class="container-v">
                    <div class="filter">
                    <div class="search">
                        
                        <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
                        <button type="button" class="fa fa-close-o remove-text"></button>
                        <button type="button" class="fa fa-to-go button-w"></button>
                       
                    </div>
                   
                </div>
                 <div class="filter-data ">
                        <div class="pt-20 pb-10">
                            <span class="fs-14 c-black ">Filter</span>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Semua</span></label>
                                </li> 
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Destinasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Berita</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Akomondasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Hotel</span></label>
                                </li>
                            </ul>
                        </div>
                    </div>
              </div>
        </div>

<div class="popup">
    <div class="content-popup">
        <button type="button" class="dismis"><i class="fa fa-times"></i></button>
        <video src="" id="video-play" autoplay="" controls=""></video>
    </div>
    <div class="area-close"></div>
</div>
<div class="popup-rad ">
    <div class="image"><img src="assets/image/galery/galery-12.png" alt="">
        <button type="button" close="popup"><i class="fa fa-close"></i></button>
    </div>
    <div class="area-close"></div>
</div>
</body>

<script id="item-news-sel" type="text/x-jQuery-tmpl">
    <a href="news.php?url=${url}">
        <div class="pade">
            <div class="list-data">
                <div class="data-image"><img src="assets/image/post_images/${gambar}" alt=""></div>
                <div class="data-desc">
                    <h5 class="capital oswald" style="font-size: 18px; color: #666;">${nama.substr(0,150)}</h5></div>
            </div>
        </div>
    </a>
</script>

 <script src="./assets/js/jquery.js"></script>
     <script src="assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tmpl.min.js"></script>
    <script src="assets/js/jquery.auto-complete.js"></script>
 <script src="./assets/js/action.js"></script>

    <script src="assets/js/destinasi.js"></script>
<script src="assets/js/news.js"></script>
<script src="assets/js/news-get.js"></script>
 <script src="ajax/send_like.js"></script>

</html>


<script>
 //pengaman url

 jQuery(document).ready(function($) {
     $(".meta-url").attr('content',document.URL);
            $(".meta-title").attr('content',$("title").text());

      setTimeout(function(){
          if ($("#page-descript").text().length =="") {
        //window.location=("error.html");
        }
    },1000)

         var rate = $(".sub-rate").attr('rating');
            $(".start").slice(0, rate).removeClass('unrate').addClass('rate');
    });   



</script>