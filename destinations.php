<?php
require ("config/get_main.php");

?>
<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="generator" content="EXploresukabumi" >
    <meta name="description" content="temukan potongan surga yang hilang  sukabumi di sukabumi ,sukabumi terkenal akan kekayaan alam dan budayanya yang mempesona setiap pasang mata ">
    <meta name="keywords" content="exploresukabumi,sukabumi,explore,pariwisata,budaya,musik,kuliner,alam,geopark,pantai,gunung,pegunungan,bebas,alam">
    <meta name="author" content="syntac">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="tQ7xEnvjr7UdaYnzNYfRL0tX3wPa6TbOwkx__s2JZgk" />
    <meta property="og:url" content="" class="meta-url">
    <meta property="og:title" content=""   class="meta-title">
    <title>Destinasi - Explorer Sukabumi</title>
    <link rel="icon" type="img/ico" href="assets/image/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/css/main.style.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/destination.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <script src="./assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tmpl.min.js"></script>
    <script src="assets/js/destinasi.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.auto-complete.js"></script>
     <script>
        jQuery(document).ready(function() {
            $(".meta-url").attr('content',document.URL);
            $(".meta-title").attr('content',$("title").text());
        });
    </script>
    <style>
        .new-dest .list-dest .rate-area 
        {
            bottom: 0 !important;
            padding-bottom: 17px !important;
            background-color: #fff;
        }
    </style>
</head>

<body>
    <header id="header" class="header header-devault">
        <div class="container">
            <div class="logo"><a href="index.php" ><img src="assets/image/logo.png" alt=""></a></div>
            <div class="navbar">
                <button class="navbar-toggle devault-button-nav" nav-show="true"><span></span><span></span><span></span></button>
                <nav>
                    <ul>
                        <li class="uppercase"><a href="index.php">beranda</a></li>
                        <li class="active uppercase"><a href="destinations.php">destinasi</a></li>
                        <li class="uppercase"><a href="gallery.php">galeri</a></li>
                        <li class="uppercase"><a href="news-list.php" class="uppercase">berita</a></li>
                        <li class="uppercase akomodasi pointer "><span  style="color: #383838 !important">akomodasi</span> 
                            <ul class="capital">
                                <li><a href="homestay.php" title="">penginapan</a> </li>
                                <li> <a href="transportation.php" title="">transportasi</a> </li>
                            </ul>
                        </li>
                        <li class="p-0 search-unres-s">
                            <button type="button" class="search-unresponsive" active="false"><i class="fa fa-search "></i></button>
                        </li>
                        <li class="search-res">
                            <div class="ci">
                                <input type="text" name="" placeholder="Cari disini">
                                <button type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div class="search-unres inactive">
        <div class="search-result ">
            <div class="container">
                <div class="research">
                    <div class="search-title"><span>Cari destinasi , dan Artikel lainya</span></div>
                    <div class="input-search">
                        <div class="ci">
                            <input type="text" name="" placeholder="Cari disini">
                            <button type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container many-data">
        <div class="heading py-50 pl-20">
            <div class="title">
                <h1 class="fs-30 uppercase text-left oswald">Destinasi Terbaik Minggu ini</h1></div>
        </div>
        <div class="other">
            <div class="other-list">
                <h1 class="tex-tleft fs-25 uppercase pb-20 oswald">gunung gede</h1>
                <p class="fs-14 text-left justify">Taman Nasional Gunung Gede Pangrango merupakan salah satu kawasan lindung di Jawa Barat yang cukup terkenal di kalangan para pendaki. Dua gunung yang saling berdampingan ini memang menjadi primadona tersendiri bagi para pendaki, salah satu alasannya adalah keberadaan alun-alun Surya Kencana yang jadi spot terbaik untuk menikmati edelweiss.</p>
                <p class="fs-14 text-left justify pt-10">Gunung Gede Pangrango masuk dalam kawasan Taman Nasional Gunung Gede Pangrango yang merupakan salah satu dari lima taman nasional yang pertama kali diumumkan di Indonesia pada tahun 1980. </p>
                <p class="fs-14 text-left justify pt-10">Gunung Gede juga memiliki keanekaragaman ekosistem yang terdiri dari formasi-formasi hutan submontana, montana, subalpin; serta ekosistem danau, rawa, dan savana.
                Gunung Gede terkenal kaya akan berbagai jenis burung yaitu sebanyak 251 jenis dari 450 jenis yang terdapat di Pulau Jawa. Beberapa jenis di antaranya merupakan burung langka yaitu elang Jawa (Spizaetus bartelsi) dan celepuk jawa (Otus angelinae).

                Taman Nasional Gunung Gede-Pangrango ditetapkan oleh UNESCO sebagai Cagar Biosfir pada tahun 1977, dan sebagai Sister Park dengan Taman Negara</p>
            </div>
            <div class="other-list"><img src="assets/image/destinations/gunung-gede.jpg" alt=""></div>
        </div>
    </div>
    <div class="new-dest">
        <div class="heading pb-50">
            <div class="title title-center">
                <h1 class="text-center fs-30 uppercase oswald">explore sukabumi</h1></div>
            <div class="subtitle">
                <p class="text-center">Temukan potongan Surga yang hilang di Sukabumi.</p>
            </div>
        </div>
        <div class="container">
            <div id="items-list"></div>
        </div>
    </div>
  <!--    footer area -->
        <footer class="footer">
            <div class="container">
                <div class="foot-logo ">
                    <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.php" title=""><img src="assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
                    <div class="some">
                        <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
                        <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3></div>
                </div>
                <div class="foot-nav">
                    <div class="nav-title mb-20">
                    
                    </div>
                    <div class="nav-body">
                        <nav>
                           <ul>
                               <li>
                                   <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.php" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.php" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.php" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.php" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.php" title="news">transportasi</a> </li>
                        </ul>
                               </li>
                                <li>
                                   <ul>
                                       <li class="capital"><span>hubungi kami</span></li>
                                       <li><p>Telepon</p></li>
                                       <li><p>+123 456 789 </p></li>
                                       <li><p>Email</p></li>
                                       <li><p>eksukabumi@hotmail.com</p></li>
                                      
                                   </ul>
                               </li>

                                <li>
                                   <ul>
                                       <li class="capital"><span>sosial media</span></li>
                                       <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                                        <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                                         <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                                          <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                                   </ul>
                               </li>
                           </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>


       
        
        <div class="search-popup inactive">
            <button type="button" class="fa fa-close-o dismis-c"></button>
              <div class="container-v">
                    <div class="filter">
                    <div class="search">
                        
                        <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
                        <button type="button" class="fa fa-close-o remove-text"></button>
                        <button type="button" class="fa fa-to-go button-w"></button>
                       
                    </div>
                   
                </div>
                 <div class="filter-data ">
                        <div class="pt-20 pb-10">
                            <span class="fs-14 c-black ">Filter</span>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Semua</span></label>
                                </li> 
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Destinasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Berita</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Akomondasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Hotel</span></label>
                                </li>
                            </ul>
                        </div>
                    </div>
              </div>
        </div>
    <div class="popup">
        <div class="content-popup">
            <button type="button" class="dismis"><i class="fa fa-times"></i></button>
            <video src="" id="video-play" autoplay="" controls=""></video>
        </div>
    </div>
</body>

<script type="text/template" id="template_temp">
   {{#destination}}
    <div class="items dest-sub-date">
        <a href="destination.php?lokasi={{url}}" title="" style="text-decoration: none;color: #666;" >
        <div class="cont-item">
            <div class="list-dest dests-data-items" >
                <div class="dest-data">
                    <div class="dest-header"> <img src="assets/image/post_images/{{gambar}}"> </div>
                </div>
                <div class="dest-desc">
                    <div class="container-list pt-0 px-10">
                        <h4 class="text-left pb-6 fs-19 oswald capital">{{nama}}</h4>
                        
                        <div class="  break lh justify description-text"  style="font-size:14px !important ">{{{deskripsi}}}</div>
                    </div>
                </div>

                <div class="rate-area">
                  <div class='rate' >
                    <div class="sub-rate" rating='{{rate}}'>
                        <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                         <div class="start unrate">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </a>
    </div>
    {{/destination}}
    
</script>
<script src="./assets/js/action.js"></script>
<script src="assets/js/mustache.js"></script>   
<script src="./assets/js/destinasi-get.js"></script>

<script>
    //this for setting rating 
function rating_get()
{
     for (var i = 1; i <= $(".dest-sub-date").length; i++) {

            var rate = $(".dest-sub-date:nth-child(" + i + ")").find(".sub-rate").attr('rating');

            $(" .dest-sub-date:nth-child(" + i + ")").find('.start').slice(0, rate).removeClass('unrate').addClass('rate');
        }
}
setInterval(function(){
rating_get()
},200)


    jQuery(document).ready(function($) {
       rating_get();
});

setTimeout(function(){
for (var i = 0; i <= $(".dest-sub-date").length; i++) {
    var descr = $(".dest-sub-date").eq(i).find('.description-text').text().substring(0,100);
    $(".dest-sub-date").eq(i).find('.description-text').empty().append("<p>" + descr + "...</p>")
}
    
},200)
</script>

</html>