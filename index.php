<?php
require ("config/get_main.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="generator" content="EXploresukabumi" >
    
    <meta name="description" content="temukan potongan surga yang hilang  sukabumi di sukabumi ,sukabumi terkenal akan kekayaan alam dan budayanya yang mempesona setiap pasang mata ">
    <meta name="keywords" content="exploresukabumi,sukabumi,explore,pariwisata,budaya,musik,kuliner,alam,geopark,pantai,gunung,pegunungan,bebas,alam">
    <meta name="author" content="syntac">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="tQ7xEnvjr7UdaYnzNYfRL0tX3wPa6TbOwkx__s2JZgk" />
    <meta property="og:url" content="" class="meta-url">
    <meta property="og:title" content=""   class="meta-title">

    <title>Beranda - Explore Sukabumi</title>
    <link rel="icon" type="img/ico" href="assets/image/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/css/main.style.css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/main.style.css">
    <script src="./assets/js/jquery.js"></script>

    <script src="assets/js/jquery.auto-complete.js"></script>
    <script type="text/javascript" src="assets/js/jquery.tmpl.min.js"></script>
    <script src="assets/js/destinasi.js"></script>
    <script>
        jQuery(document).ready(function() {
            $(".meta-url").attr('content',document.URL);
            $(".meta-title").attr('content',$("title").text());
        });
    </script>
</head>

<body>
    <header id="header" class="header header-devault">
        <div class="container">
            <div class="logo"><a href="index.php" ><img src="assets/image/logo.png" alt=""></a></div>
            <div class="navbar">
                <button class="navbar-toggle devault-button-nav" nav-show="true"><span></span><span></span><span></span></button>
                <nav>
                    <ul>
                        <li class="active uppercase" class="active"><a href="index.php">beranda</a></li>
                        <li class="uppercase"><a href="destinations.php">destinasi</a></li>
                        <li class="uppercase"><a href="gallery.php" class="uppercase">galeri</a></li>
                        <li class="uppercase"><a href="news-list.php" class="uppercase">berita</a></li>
                        <li class="uppercase akomodasi pointer "><span>akomodasi</span> 
                            <ul class="capital">
                                <li><a href="homestay.php" title="">penginapan</a> </li>
                                <li> <a href="transportation.php" title="">transportasi</a> </li>
                            </ul>
                        </li>
                        <li class="p-0 search-unres-s">
                            <button type="button" class="search-unresponsive" active="false"><i class="fa fa-search "></i></button>
                        </li>

                        <li class="search-res">
                            <div class="ci">
                                <input type="text" name="" placeholder="Cari disini">
                                <button type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    
    <section class="banner mb-0">
        <div class="banner-top">
            <div class="banner-left ">
                <div class="banner-data">
                    <div class="banner-top-desc"><span class="pick uppercase fade fs-13"><b>Pick Out more</b></span></div>
                    <div class="banner-title py-10">
                        <h1 class="uppercase oswald ">Ujung genteng</h1></div>
                    <div class="banner-desc pt-5">
                        <p class="fs-18 pharagrap-l lh" style="color: #444">Salah satu pantai persinggahan para penyu ini menyuguhkan pemandangan pantai yang sangat indah dan mempesona yang menjadikan pantai ini patut diunjungi</p>
                    </div>
                    <div class="pt-30 button-action">
                        <a id="target-link" href="destination.php?lokasi=ujung-genteng">
                            <button type="button " class="uppercase py-10 px-25 fs-14">EXPLORE</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="banner-right"></div>
        </div>
        <div class="container">
            <div class="selection-banner">
                <div id="banner-list" class="owl-carousel selector">
                    <div class="item list-banner create">
                        <div class="square">
                            <div><span class="fs-14 square-title"></span></div>
                        </div>
                        <div class="content-list"><span class="list-title to-title ">Ujung genteng</span>
                            <p class="fade uppercase fs-11">Pantai</p>
                            <div class="hiden"><span class="to-desc-title">Salah satu pantai persinggahan para penyu ini menyuguhkan pemandangan pantai yang sangat indah dan mempesona yang menjadikan pantai ini patut diunjungi</span><span class="to-img">banner-ujung-genteng.png</span><span class="target-data-link">destination.php?lokasi=ujung-genteng</span></div>
                        </div>
                    </div>

                    <div class="item list-banner ">
                        <div class="square">
                            <div><span class="fs-14 square-title"></span></div>
                        </div>
                        <div class="content-list"><span class="list-title to-title ">Curug Awang</span>
                            <p class="fade uppercase fs-11">Air Terjun</p>
                            <div class="hiden"><span class="to-desc-title">Curug Awang merupakan salah satu curug yang berada di aliran sungai Ciletuh, sungai ini membatasi wilayah kecamatan Ciemas dengan kecamatan Ciracap, disamping itu juga merupakan batas antara desa Taman Jaya</span><span class="to-img">banner-curug-awang.png</span><span class="target-data-link">destination.php?lokasi=curug-awang</span></div>
                        </div>
                    </div>
                    <div class="item list-banner ">
                        <div class="square">
                            <div><span class="fs-14 square-title"></span></div>
                        </div>
                        <div class="content-list"><span class="list-title to-title ">Geopark Ciletuh</span>
                            <p class="fade uppercase fs-11">Geopark</p>
                            <div class="hiden"><span class="to-desc-title">Geopark nasional kebanggaan indonesia yang berada disukabumi ini menyimpan banyak keindahan alam yang luar biasa dan keragaman flora yang variatif</span><span class="to-img">banner-curug-cikanteh-geopark.png</span><span class="target-data-link">destination.php?lokasi=geopark-ciletuh</span></div>
                        </div>
                    </div>
                    <div class="item list-banner ">
                        <div class="square">
                            <div><span class="fs-14 square-title"></span></div>
                        </div>
                        <div class="content-list"><span class="list-title to-title ">Gunung Gede</span>
                            <p class="fade uppercase fs-11">Cagar Alam</p>
                            <div class="hiden"><span class="to-desc-title">Taman nasional gunung gede pangrango destinasi yang patut untuk dikunjungi, ditetapkan UNESCO sebagai Cagar Biosfir pada tahun 1977, dan sebagai Sister Park dengan Taman Negara</span><span class="to-img">banner-gunung-gede.png</span><span class="target-data-link">destination.php?lokasi=gunung-gede</span></div>
                        </div>
                    </div>
                    <div class="item list-banner ">
                        <div class="square">
                            <div><span class="fs-14 square-title"></span></div>
                        </div>
                        <div class="content-list"><span class="list-title to-title ">Kampung Ciptagelar</span>
                            <p class="fade uppercase fs-11">kampung adat</p>
                            <div class="hiden"><span class="to-desc-title">Alam selalu memberikan yang terbaik ketika kita mampu merawatnya. Ini tentang mereka,para ‘pelestari padi’ di lereng bukit bagian selatan Taman Nasional Gunung Halimun, Kampung Ciptagelar, Sukabumi</span><span class="to-img">banner-kampung-ciptagelar.png</span><span class="target-data-link">destination.php?lokasi=kampung-ciptagelar</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="destination">
        <div class="container py-20 pl-10">
            <div class="dev-title title py-20">
                <h2 class="oswald fs-35">DESTINASI KAMI</h2></div>
        </div>
        <div class="container mb-50">
            
                <div id="destination" class="owl-carousel">
                   
                </div>
        </div>
        </div>
        <div class="container pb-100">
            <div class="dest-data ">
                <div class="direct-list">
                    <div class="list-dest">
                        <div class=" container-list center-image-dest ">
                            <div class="container-list">
                                <h6 class="text-center fade uppercase fs-14"><b>PANTAI</b></h6>
                                <div class="text-title-data title-center py-10 " style="overflow: visible">
                                    <h1 class="text-center oswald">UJUNG GENTENG</h1></div>
                                <div class="text-title-meta-data">
                                    <h6 class="text-center uppercase fs-14 fade"><b>Persinggahan para Penyu</b></h6></div>
                                <div class="description-title py-10 px-18 lh pt-40 justify ">
                                    <p>Di area pesisir selatan Jawa barat, tepatnya diwilayah sukabumi ada pantai yang indah untuk disambangi, yaitu Pantai Ujung Genteng, sebuah pantai yang ada di pesisir Sukabumi yang berbatasan langsung dengan dahsyatnya laut lepas samudera Indonesia.</p>
                                    <p class="pt-20">nama ujung genteng ini sebenarnya merupakakan perubahan ejaan dari nama ujung gunting <a href="destination.php?lokasi=ujung-genteng" title="" style="color: #424242; text-decoration: underline;">selengkapnya...</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-dest">
                        <div class="container-list">
                            <div class="container-bg">
                                <div>
                                    <div class="bg-desc-1 zoom"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="direct-list">
                    <div class="container-list ">
                        <div class="">
                            <div class="container-list">
                                <div class="map b-b py-10"><img src="assets/image/map-ujung-genteng.png"></div>
                                <div class="container-bg">
                                    <div class="dest-desc">
                                        <div class="back-img-uj"></div>
                                    </div>
                                    <div class="desc-fly pl-40"><span class="fs-14">Flying at the sky, fell free</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="destination ">
            <div class="container">
                <div class="dest-list">
                    <div class="container-list">
                        <div class="desc-list px-10 b-b">
                            <div class="container-list">
                                <div class="heading">
                                    <div class="since">
                                        <h6 class=" subtitle since fade uppercase fs-12"><b>Since 1997</b></h6></div>
                                    <div class="dest-title title-right">
                                        <h1 class="title uppercase fs-30 oswald">geopark ciletuh</h1></div>
                                    <!-- <div class="subtitle fs-13"><h6>subtitle after heading</h6></div>--></div>
                                <div class="pharagrap pt-40 lh">
                                    <p class="fs-14">Geopark nasional kebanggaan indonesia yang berada disukabumi ini menyimpan banyak keindahan alam yang luar biasa dan keragaman flora yang variatif <a href="destination.php?lokasi=geopark-ciletuh" title="" style="color: #424242; text-decoration: underline;">selengkapnya...</a></p>
                                </div>
                                <div class="image-date">
                                    <div class="list-image">
                                        <div id="list-image" class="owl-carousel">
                                            <div class="item image-list">
                                                <div class="container-bg">
                                                    <div><img src="assets/image/geopark-ciletuh-img.png" alt="" class="zoom"></div>
                                                </div>
                                            </div>
                                            <div class="item image-list">
                                                <div class="container-bg">
                                                    <div><img src="assets/image/geopark-ciletuh-sodong.png" alt="" c class="zoom"></div>
                                                </div>
                                            </div>
                                            <div class="item image-list">
                                                <div class="container-bg">
                                                    <div><img src="assets/image/geopark-cikaso.jpg" alt="" class="zoom"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="desc-list px-10 b-b">
                            <div class="container-bg">
                                <div>
                                    <div class="bg" id="bg-list"><img src="assets/image/geopark-ciletuh.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dest-list px-10 b-b">
                    <div class="container-list "><img src="assets/image/geopark-map.png" alt=""></div>
                </div>
            </div>
            <div class="play-ground ">
                <div class="heading py-20 panorama">
                    <div class="title title-center panorama py-20">
                        <h1 class="text-center uppercase fs-30 oswald">panorama sukabumi</h1> </div>
                </div>
                <div class="body">
                    <div class="cover">
                        <div class="button">
                            <button type="button" class="animate-scale-z" target-video="assets/video/video.mp4" id="video-feed"><i class="fa fa-play "></i> </button>
                        </div>
                    </div>
                    <div class="over-text">
                        <h3 class="text-center " style="line-height: 1.7"> <p class="fs-16 c-white">Ini semua tentang bagaimana kita mensyukuri apa yang tuhan berikan kapada kita</p><p class="fs-16 c-white">maka jagalan karunia itu</p></h3> </div>
                </div>
            </div>
        </div>
        <div class="destination-3 py-30 ">
            <div class="container">
                <div class="dest-list">
                    <div class="desc-list">
                        <div class="">
                            <div class=" ">
                                <div class="info-left">
                                    <div class="heading pb-20 pl-10">
                                        <div class="since">
                                            <h6 class="uppercase fs-14 fade"><b>pantai</b></h6></div>
                                        <div class="title py-10">
                                            <h1 class="fs-30 uppercase oswald">Palabuhan ratu</h1></div>
                                        <div class="subtitle">
                                            <h6 class="fs-16 " style="color:#666" >Pantai selatan</h6></div>
                                    </div>
                                    <div class="description pr-10">
                                        <div class="description-target pb-20 lh ">
                                            <p class="fs-14 lh-2">Palabuhan Ratu merupakan ibu kota Kabupaten Sukabumi Pada masa Hindia Belanda, daerah ini dikenal dengan nama Wijnkoops-baai. Letaknya yang berada di pesisir Samudra Hindia, yakni di bagian barat daya wilayah kabupaten, menyuguhkan sejumlah pesona pantai yang mengagumkan.</p>
                                            <p class="fs-14 lh-2 pt-20 ">Lokasinya terletak sekitar 60 km ke arah selatan dari Kota Sukabumi. Pantai ini dikenal memiliki ombak yang sangat kuat dan karena itu berbahaya bagi perenang pantai. Topografinya berupa perpaduan antara pantai yang curam dan landai, tebing karang terjal, hempasan ombak, dan hutan cagar alam <a href="destination.php?lokasi=palabuhan-ratu" title="" style="color: #424242; text-decoration: underline;">selengkapnya...</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="desc-image pt-10">
                                <div class="container-bg box-shadow">
                                    <div class="sec"><img src="assets/image/pelabuhan-raja2.jpg " class="zoom"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dest-list pt-10 b-b">
                    <div class="desc-info">
                        <div class="container-bg bg-white box-shadow">
                            <div> <img src="assets/image/pelabuhan-raja.jpg " class="zoom" alt=""> </div>
                        </div>
                    </div>
                    <div class="dest-info-2 "><img src="assets/image/pelabuhan-map.png" class="image-map" alt=""></div>
                </div>
            </div>
        </div>
        <!-- <div class="subscribe"><div class="heading py-40"><div class="title-center"><h1 class="text-center fs-30 uppercase">subscribe</h1></div><div class="subtitle"><h6 class="text-center fs-14">Lorem ipsum dolor sit amet, consectetur</h6></div></div><div class="body-sub"><form method="get" accept-charset="utf-8"> <input type="text" name="" placeholder="Email Addres" class="action-form"><div class="pl-7"><button type="submit" class=" px-25 py-12 bg-yellow">Send</button></div></form></div></div>-->
        <a href="gallery.php" title="" class="td-none">
        <div class="some-article py-20  pt-40">
            <div class="container">
                <div class="heading pb-30 pl-10">
                    <div class="title pointer">
                        <h1 class="fs-30 uppercase oswald ">galeri</h1></div>
                </div>
            </div>
            <div class="container pt-30 pb-10">
                <div id="owl-galery" class="owl-carousel">
                    <div class="item hover-pointer">
                        <div class="image-list zoom-to-image"><img src="assets/image/article.jpg" alt="">
                            <div class="image-cover"></div>
                        </div>
                        <div class="image-desc px-15">
                            <p class="fs-14"></p>
                            <p class="fs-14"></p>
                        </div>
                    </div>
                    <div class="item hover-pointer">
                        <div class="image-list zoom-to-image"><img src="assets/image/pelabuhan-raja2.jpg" alt="">
                            <div class="image-cover"></div>
                        </div>
                        <div class="image-desc px-15">
                            <p class="fs-14"></p>
                            <p class="fs-14"></p>
                        </div>
                    </div>
                    <div class="item hover-pointer">
                        <div class="image-list zoom-to-image"><img src="assets/image/cikanteh1.jpg" alt="">
                            <div class="image-cover"></div>
                        </div>
                        <div class="image-desc px-15">
                            <p class="fs-14"></p>
                            <p class="fs-14"></p>
                        </div>
                    </div>
                    <div class="item hover-pointer">
                        <div class="image-list zoom-to-image"><img src="assets/image/destination-data.png" alt="">
                            <div class="image-cover"></div>
                        </div>
                        <div class="image-desc px-15">
                            <p class="fs-14"></p>
                            <p class="fs-14"></p>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>
    </a>
  <!--    footer area -->
        <footer class="footer">
            <div class="container">
                <div class="foot-logo ">
                    <h2 class="text-left fs-25 c-white uppercase py-10"><a href="index.php" title=""><img src="assets/image/logo-invert.png" class="h-60 logo logo-image pointer"></a> </h2>
                    <div class="some">
                        <h3 class="text-left py-5 fs-14 c-white ">2018</h3>
                        <h3 class="textleft py-5 c-white fs-14">ALL right reserved</h3></div>
                </div>
                <div class="foot-nav">
                    <div class="nav-title mb-20">
                    
                    </div>
                    <div class="nav-body">
                        <nav>
                           <ul>
                               <li>
                                  <ul>
                           <li><span>Navigasi</span></li>
                           <li class="capital"><a href="destinations.php" title="destinations">destinasi</a> </li>
                           <li class="capital"><a href="gallery.php" title="gallery">galeri</a> </li>
                           <li class="capital"><a href="news-list.php" title="news">berita</a> </li>
                           <li class="capital"><a href="homestay.php" title="news">penginapan</a> </li>
                           <li class="capital"><a href="transportation.php" title="news">transportasi</a> </li>
                        </ul>
                               </li>
                                <li>
                                   <ul>
                                       <li class="capital"><span>hubungi kami</span></li>
                                       <li><p>Telepon</p></li>
                                       <li><p>+123 456 789 </p></li>
                                       <li><p>Email</p></li>
                                       <li><p>eksukabumi@hotmail.com</p></li>
                                      
                                   </ul>
                               </li>

                                <li>
                                   <ul>
                                       <li class="capital"><span>sosial media</span></li>
                                       <li><a href="https://instagram.com" title="instagram">Instagram</a> </li>
                                        <li><a href="https://facebook.com" title="Facebook">Facebook</a> </li>
                                         <li><a href="https://twitter.com" title="twitter">Twitter</a> </li>
                                          <li><a href="https://youtube.com" title="youtube">Youtube</a> </li>
                                   </ul>
                               </li>
                           </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>

                    <div class="load-me-first loader-data hiden ">
                        <img src="assets/image/banner-ujung-genteng.png" alt="">
                         <img src="assets/image/banner-curug-awang.png" alt="">
                          <img src="assets/image/banner-curug-cikanteh-geopark.png" alt="">
                           <img src="assets/image/banner-gunung-gede.png" alt="">
                           <img src="assets/image/banner-kampung-ciptagelar.png" alt="">
                    </div>

       
        
        <div class="search-popup inactive">
            <button type="button" class="fa fa-close-o dismis-c"></button>
              <div class="container-v">
                    <div class="filter">
                    <div class="search">
                        
                        <input type="text" name="" class="text-input-filter" placeholder="Cari destinasi atau berita disini">
                        <button type="button" class="fa fa-close-o remove-text"></button>
                        <button type="button" class="fa fa-to-go button-w"></button>
                       
                    </div>
                   
                </div>
                 <div class="filter-data ">
                        <div class="pt-20 pb-10">
                            <span class="fs-14 c-black ">Filter</span>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Semua</span></label>
                                </li> 
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Destinasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Berita</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Akomondasi</span></label>
                                </li>
                                <li><label> <input type="radio" name="list-change"><span class="radio"></span>
                                    <span class="to-data px-10">Hotel</span></label>
                                </li>
                            </ul>
                        </div>
                    </div>
              </div>
        </div>


        <!--scrolltop area -->

        <div class="to-top unscroll" id="to-top">
            <div class="button">
                <button type="" >  <i class="fa fa-chevron-up "></i> </button>
            </div>
        </div>
        <!--   popup area  -->

       




        <div class="popup">
            <div class="content-popup">
                <button type="button" class="dismis"><i class="fa fa-times"></i></button>
                <video src="" id="video-play" autoplay="" controls=""></video>
            </div>
            <div class="area-close"></div>
        </div>

</body>

<script src="ajax/ajax_visitor.js"></script>

   <script id="item-destinations-tmpl" type="text/x-jQuery-tmpl">
                    <div class="item destination-data " >
                        <a style="text-decoration: none; color:#424242 " href="destination.php?lokasi=${url}">
                        <div class="destination-list">
                            <div class="destination-header">
                                <div> </div>
                            </div>
                            <div class="destination-content">
                                <div class="desc-content">
                                    <div> <img src="assets/image/post_images/${gambar}" alt="">
                                        <div class="bg-image-desc"></div>
                                    </div>
                                </div>
                                <div class="desc-content-overlay">
                                    <div class="desc">
                                        <div>
                                            <h5 class="desc-title fs-18 oswald">${nama}</h5>
                                            <p class="desc-pharagrap fs-13 lh capital break text-left">${deskripsi.substr(20,90)}...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
</script>
    <script src="assets/js/owl.carousel.js"></script>
<script src="./assets/js/action.js"></script>
<script>
 

  $.ajax({
            url: 'config/show_post_destination.php',
        success:function(result)
        {
            var destinasi = $.parseJSON(result);
    

$.each( doRandom(destinasi.slice(0,10)), function (key, data) {
    $("#item-destinations-tmpl").tmpl(data).appendTo("#destination");
});

}});

</script>

</html>

<!--   syntac junior our team                       -->
<!--   indra harta kenda :front end developer  / https://www.facebook.com/RandyS69   ------->
<!--   gunaldi yunus     :front end developer  / https://www.facebook.com/gunaldi.yunus.79 ------->   
<!--   tofik hidayat     :front end eveloper   / https://web.facebook.com/profile.php?id=100009938049130&ref=br_rs -------> 

